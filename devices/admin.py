from django.contrib import admin
from devices.models import *

# Register your models here.
admin.site.register(Technology)
admin.site.register(DeviceType)
admin.site.register(Model)
admin.site.register(Device)
admin.site.register(DeviceDetail)
