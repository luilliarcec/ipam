""" Device Forms """

# Django
from django import forms
# App
from devices.models import Device, Model, DeviceType


class DeviceForm(forms.ModelForm):
    """Formulario y validacion de device"""
    name = forms.CharField(min_length=2)
    serie = forms.CharField(min_length=2)
    model = forms.ModelChoiceField(required=True, queryset=Model.objects.all())
    device_type = forms.ModelChoiceField(required=True, queryset=DeviceType.objects.all())

    def clean(self):
        cleaned_data = super(DeviceForm, self).clean()
        return cleaned_data

    class Meta:
        model = Device
        fields = ['name', 'serie', 'model', 'device_type']


class AddressForm(forms.ModelForm):
    """Formulario y validacion de device"""
    address = forms.CharField(min_length=8)
    interface = forms.CharField(min_length=2)

    class Meta:
        model = Device
        fields = ['address']
