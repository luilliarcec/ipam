""" Device Delete Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from devices.models import Device, DeviceType, Model, Technology


class DeleteDeviceTest(TestCase):
    """Prueba que se elimine una Device"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_device(self):
        """Prueba la eliminación de una Device"""
        self.device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )
        self.model = Device.objects.create(name='Device Faker', device_type=self.device_type, model=model, serie=1111)

        self.client.delete('/device/delete/{}'.format(self.model.pk))

        self.assertFalse(Device.objects.filter(name='Device Faker').exists())
