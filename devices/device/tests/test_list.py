""" Device List Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Device, DeviceType, Model, Technology


class ListDeviceTest(TestCase):
    """Prueba la vista index de Device"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )

        self.device1 = Device.objects.create(
            name='Device One', device_type=device_type, model=model, serie=4005,
            created_at=datetime(2020, 2, 10, 19, 30, 19, 000000)
        )

        self.device2 = Device.objects.create(
            name='Device Two', device_type=device_type, model=model, serie=1024,
            created_at=datetime(2020, 2, 11, 19, 30, 19, 000000)
        )

        self.device3 = Device.objects.create(
            name='Device Three', device_type=device_type, model=model, serie=2048,
            created_at=datetime(2020, 2, 13, 19, 28, 41, 000000)
        )

    def test_that_you_can_see_list_of_devices(self):
        """Prueba que las Device se puedan ver paginadas en el template"""
        response = self.client.get('/device/')
        self.assertContains(response, self.device1.name)
        self.assertContains(response, self.device2.name)
        self.assertContains(response, self.device3.name)

    def test_that_it_can_be_filtered_by_address_name(self):
        """Prueba que las Device se puedan filtrar por nombre"""
        response = self.client.get('/device/?search=on')
        self.assertNotContains(response, self.device3.name)
        self.assertNotContains(response, self.device2.name)
        self.assertContains(response, self.device1.name)
