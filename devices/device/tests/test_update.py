""" Device Update Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from devices.models import Device, DeviceType, Model, Technology


class UpdateDeviceTest(TestCase):
    """Prueba que se cree la Device"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )
        self.model = Device.objects.create(name='Device Faker', device_type=self.device_type, model=model, serie=1111)

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/device/edit/{}'.format(self.model.pk), {
            'name': 'A',
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Device.objects.filter(name='A').exists())

    def test_validate_that_the_serie_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/device/edit/{}'.format(self.model.pk), {
            'serie': 'A',
        })

        self.assertFormError(
            response,
            'form',
            'serie',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Device.objects.filter(serie='A').exists())

    def test_validate_that_the_model_and_type_exists_in_databse(self):
        """Valida que haya un error en name"""
        response = self.client.post('/device/edit/{}'.format(self.model.pk), {
            'model': 300,
            'device_type': 300,
        })

        self.assertFormError(
            response, 'form', 'model', 'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

        self.assertFormError(
            response, 'form', 'device_type', 'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

    def test_update_a_device(self):
        """Prueba la creación de una nueva Device"""
        tech = Technology.objects.create(name='15 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Edicion Limitada', memory=5555,
            interface_amount=11, architecture='Arq Faker',
        )

        self.client.post('/device/edit/{}'.format(self.model.pk), {
            'name': 'Device One Plus',
            'serie': 'ABCD1023',
            'model': model.pk,
            'device_type': self.device_type.pk
        })
        self.assertTrue(Device.objects.filter(name='Device One Plus').exists())
