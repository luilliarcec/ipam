""" Device Views """

# Django
from django.http import JsonResponse, HttpResponseNotFound, HttpResponseBadRequest
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
# App
from django.views.generic.base import View

from utils.mixins import OldDataMixin
from devices.device.forms import DeviceForm, AddressForm
from devices.models import Device, Model, DeviceType
from networks.models import IpAddress
from utils.conexion import Info
from utils.render_pdf import Render


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista las Devices"""
    template_name = 'devices/devices/index.html'
    model = Device
    paginate_by = 15
    context_object_name = 'devices'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Device.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Show(LoginRequiredMixin, DetailView):
    """Muestra el detalle del dispositivo"""
    template_name = 'devices/devices/show.html'
    model = Device
    context_object_name = 'device'
    info = Info()

    def get_context_data(self, **kwargs):
        context = super(Show, self).get_context_data(**kwargs)
        try:
            ip_address = IpAddress.objects.filter(device=self.get_object()).first()
            context['informations'] = [self.info.get_status(target=ip_address.address)]
            context['interfaces'] = self.info.get_interface(target=ip_address.address)
            context['addresses'] = self.info.get_ip_address(target=ip_address.address)
        except Exception as ex:
            print(ex)
            context['informations'] = []
            context['interfaces'] = []
            context['addresses'] = []

        return context


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea una Device"""
    model = Device
    template_name = 'devices/devices/create.html'
    form_class = DeviceForm
    success_url = reverse_lazy('devices:device.index')
    attributes = {'name': '', 'serie': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_model'] = self.post_old_data('model')
        context['models'] = Model.objects.all()
        context['old_device_type'] = self.post_old_data('device_type')
        context['device_types'] = DeviceType.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza una Device"""
    model = Device
    template_name = 'devices/devices/edit.html'
    form_class = DeviceForm
    success_url = reverse_lazy('devices:device.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'serie': self.get_object().serie,
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_model'] = self.post_old_data('model', self.get_object().model.pk)
        context['models'] = Model.objects.all()
        context['old_device_type'] = self.post_old_data('device_type', self.get_object().device_type.pk)
        context['device_types'] = DeviceType.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina una Device"""
    model = Device
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)


class AddAddress(LoginRequiredMixin, UpdateView, OldDataMixin):
    template_name = 'devices/devices/add-address.html'
    model = Device
    form_class = AddressForm
    context_object_name = 'device'
    info = Info()

    def form_valid(self, form):
        """PON EL CÓDIGO DE AGREGAR IPS AQUÍ"""
        ip_address_connection = IpAddress.objects.filter(device=self.get_object()).first()
        self.success_url = reverse_lazy('devices:device.add.address', args=[self.get_object().id])
        try:
            self.info.add_ip_address(str(ip_address_connection), self.request.POST['address'],
                                     self.request.POST['interface'], '')
        except Exception as ex:
            print(ex)
            return HttpResponseBadRequest('¡IP no válida!')

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AddAddress, self).get_context_data(**kwargs)
        try:
            ip_address = IpAddress.objects.filter(device=self.get_object()).first()
            context['informations'] = [self.info.get_status(target=ip_address.address)]
            context['interfaces'] = self.info.get_interface(target=ip_address.address)
            context['addresses'] = self.get_ip_and_mask(target=ip_address.address)
        except Exception as ex:
            print(ex)
            context['informations'] = []
            context['interfaces'] = []
            context['addresses'] = []
        return context

    def get_ip_and_mask(self, target):
        addresses = []
        for addresses_device in self.info.get_ip_address(target=target):
            address = addresses_device.address.split('/')
            addresses_device.ip = address[0]
            addresses_device.mask = address[1]
            addresses.append(addresses_device)
        return addresses


class DeleteAddress(LoginRequiredMixin, DeleteView):
    """Elimina una Device"""
    http_method_names = ['delete']
    model = Device
    info = Info()

    def get_queryset(self):
        return Device.objects.get(id=self.kwargs.get('pk'))

    def delete(self, request, *args, **kwargs):
        """PON EL CÓDIGO DE ELIMINACIÓN AQUÍ"""
        num = kwargs.get('num')
        ip_address_connection = IpAddress.objects.filter(device_id=kwargs.get('pk')).first()

        if ip_address_connection is None:
            return HttpResponseNotFound('¡Dispositivo no encontrado!')

        try:
            self.info.delete_ip_adress(str(ip_address_connection), num)
        except Exception as ex:
            print(ex)
            return HttpResponseNotFound('¡IP no encontrada!')

        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)


class ReportDevice(LoginRequiredMixin, View):
    title = 'Reporte de equipos'

    def get(self, request):
        devices = Device.objects.all()
        today = timezone.now()
        params = {
            'title': self.title,
            'today': today,
            'devices': devices,
            'request': request
        }
        return Render.render('devices/devices/reports/list.html', params)


class ReportDeviceDetail(LoginRequiredMixin, View):
    title = 'Reporte de información del equipo'
    template_name = 'devices/devices/reports/detail.html'
    info = Info()

    def get(self, request, **kwargs):
        params = {
            'title': self.title,
            'today': timezone.now(),
            'request': request
        }

        try:
            ip_address = IpAddress.objects.filter(device_id=kwargs.get('pk')).first()
            params['informations'] = [self.info.get_status(target=ip_address.address)]
            params['interfaces'] = self.info.get_interface(target=ip_address.address)
            params['addresses'] = self.info.get_ip_address(target=ip_address.address)
        except Exception as ex:
            print(ex)
            params['informations'] = []
            params['interfaces'] = []
            params['addresses'] = []

        return Render.render(self.template_name, params)
