# Generated by Django 2.2.7 on 2020-02-16 18:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('racks', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('serie', models.CharField(max_length=50, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Dispositivo',
                'verbose_name_plural': 'Dispositivos',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='DeviceInformation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.CharField(max_length=50)),
                ('os_version', models.CharField(max_length=50)),
                ('architecture', models.CharField(max_length=50)),
                ('uptime', models.CharField(max_length=50)),
                ('cpu', models.CharField(max_length=50)),
                ('cpu_frequency', models.CharField(max_length=50)),
                ('serie', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Información de dispositivo',
                'verbose_name_plural': 'Información de dispositivos',
                'ordering': ('model',),
            },
        ),
        migrations.CreateModel(
            name='DeviceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('description', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Tipo de dispositivo',
                'verbose_name_plural': 'Tipos de Dispositivo',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='InterfazInformation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('type', models.CharField(max_length=50)),
                ('mtu', models.CharField(max_length=50)),
                ('mac', models.CharField(max_length=50)),
                ('ldown', models.CharField(max_length=50)),
                ('lup', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Información de interfaz',
                'verbose_name_plural': 'Información de interfaces',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='IpInformation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=50)),
                ('address', models.CharField(max_length=50)),
                ('network', models.CharField(max_length=50)),
                ('interfaz', models.CharField(max_length=50)),
                ('dhcp', models.CharField(max_length=50)),
                ('state', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Información de IP',
                'verbose_name_plural': 'Información de IPs',
                'ordering': ('address',),
            },
        ),
        migrations.CreateModel(
            name='Technology',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('maker', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Tecnología',
                'verbose_name_plural': 'Tecnologías',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('memory', models.CharField(max_length=50)),
                ('interface_amount', models.CharField(max_length=50)),
                ('architecture', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('technology', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='devices.Technology')),
            ],
            options={
                'verbose_name': 'Modelo',
                'verbose_name_plural': 'Modelos',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='DeviceDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', models.CharField(max_length=50)),
                ('installed_at', models.DateField(default=0)),
                ('change_at', models.DateField(default=0)),
                ('ur_number', models.CharField(max_length=50)),
                ('ur_amount', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('device', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='devices.Device')),
                ('rack', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='racks.Rack')),
            ],
            options={
                'verbose_name': 'Detalle de dispositivo',
                'verbose_name_plural': 'Detalles de dispositivo',
                'ordering': ('device',),
            },
        ),
        migrations.AddField(
            model_name='device',
            name='device_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='devices.DeviceType'),
        ),
        migrations.AddField(
            model_name='device',
            name='model',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='devices.Model'),
        ),
    ]
