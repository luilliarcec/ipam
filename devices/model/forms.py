""" Models Forms """

# Django
from django import forms
# App
from devices.models import Model, Technology


class ModelForm(forms.ModelForm):
    """Formulario y validacion de modelo de dispositivos"""
    technology = forms.ModelChoiceField(required=True, queryset=Technology.objects.all())
    name = forms.CharField(min_length=2)
    memory = forms.IntegerField()
    interface_amount = forms.IntegerField()

    def clean(self):
        cleaned_data = super(ModelForm, self).clean()
        return cleaned_data

    class Meta:
        model = Model
        fields = ['name', 'memory', 'interface_amount', 'architecture', 'technology']
