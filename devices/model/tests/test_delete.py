""" Models Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Technology, Model


class DeleteModelTest(TestCase):
    """Prueba que se elimine un modelo"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_battery(self):
        """Prueba la eliminación de un modelo"""
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )

        self.client.delete('/device/model/delete/{}'.format(model.pk))

        self.assertFalse(Model.objects.filter(name='Modelo Faker').exists())