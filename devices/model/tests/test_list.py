""" Models Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Technology, Model


class ListModelTest(TestCase):
    """Prueba la vista index de models"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')

        self.model_1 = Model.objects.create(
            technology=tech,
            name='Huaweii',
            memory=1024,
            interface_amount=14,
            architecture='Win',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.model_2 = Model.objects.create(
            technology=tech,
            name='Mikrotik',
            memory=1024,
            interface_amount=14,
            architecture='Win',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.model_3 = Model.objects.create(
            technology=tech,
            name='Ubiquiti',
            memory=1024,
            interface_amount=14,
            architecture='Win',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_model(self):
        """Prueba que las Modelos se puedan ver paginadas en el template"""
        response = self.client.get('/device/model')
        self.assertContains(response, self.model_3.name)
        self.assertContains(response, self.model_2.name)
        self.assertContains(response, self.model_1.name)

    def test_that_it_can_be_filtered_by_model_name(self):
        """Prueba que las Modelos se puedan filtrar por nombre"""
        response = self.client.get('/device/model?search=huaw')
        self.assertNotContains(response, self.model_3.name)
        self.assertNotContains(response, self.model_2.name)
        self.assertContains(response, self.model_1.name)
