""" Models Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Technology, Model


class UpdateModelTest(TestCase):
    """Prueba que se actualize el modelo"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        self.model = Model.objects.create(
            technology=self.tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/device/model/edit/{}'.format(self.model.pk), {
            'name': 'U',
            'memory': 1024,
            'interface_amount': 14,
            'architecture': 'Win',
            'technology': self.tech.pk,
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Model.objects.filter(name='U').exists())

    def test_validate_that_memory_is_integer(self):
        """Valida que haya un error en memory"""
        response = self.client.post('/device/model/edit/{}'.format(self.model.pk), {
            'name': 'Modelo 1',
            'memory': '20A',
            'interface_amount': 14,
            'architecture': 'Win',
            'technology': self.tech.pk,
        })

        self.assertFormError(response, 'form', 'memory', 'Introduzca un número entero.')

    def test_validate_that_interface_amount_is_integer(self):
        """Valida que haya un error en interface_amount"""
        response = self.client.post('/device/model/edit/{}'.format(self.model.pk), {
            'name': 'Modelo 1',
            'memory': 14,
            'interface_amount': '28A',
            'architecture': 'Win',
            'technology': self.tech.pk,
        })

        self.assertFormError(response, 'form', 'interface_amount', 'Introduzca un número entero.')

    def test_validate_that_all_fields_are_require(self):
        """Valida que todos los campos sean requeridos"""
        response = self.client.post('/device/model/edit/{}'.format(self.model.pk), {
            'name': '',
            'memory': '',
            'interface_amount': '',
            'architecture': '',
            'technology': '',
        })

        self.assertFormError(response, 'form', 'name', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'memory', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'interface_amount', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'architecture', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'technology', 'Este campo es obligatorio.')

    def test_validate_that_technology_exists_in_database(self):
        """Valida que haya un error en technology"""
        response = self.client.post('/device/model/edit/{}'.format(self.model.pk), {
            'name': 'Modelo 1',
            'memory': 14,
            'interface_amount': 5,
            'architecture': 'Win',
            'technology': 30,
        })

        self.assertFormError(
            response,
            'form',
            'technology',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

    def test_update_a_model(self):
        """Prueba la creación de un nuevo modelo"""
        self.client.post('/device/model/edit/{}'.format(self.model.pk), {
            'name': 'Modelo 1',
            'memory': 14,
            'interface_amount': 5,
            'architecture': 'Win',
            'technology': self.tech.pk,
        })
        self.assertTrue(Model.objects.filter(name='Modelo 1').exists())