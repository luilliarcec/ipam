""" Models Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from devices.model.forms import ModelForm
from devices.models import Model, Technology


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista las modelos"""
    template_name = 'devices/models/index.html'
    model = Model
    paginate_by = 15
    context_object_name = 'models'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Model.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea una modelo"""
    model = Model
    template_name = 'devices/models/create.html'
    form_class = ModelForm
    success_url = reverse_lazy('devices:model.index')
    attributes = {'memory': '', 'name': '', 'interface_amount': '', 'architecture': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_technology'] = self.post_old_data('technology')
        context['technologies'] = Technology.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza una modelo"""
    model = Model
    template_name = 'devices/models/edit.html'
    form_class = ModelForm
    success_url = reverse_lazy('devices:model.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'memory': self.get_object().memory,
            'interface_amount': self.get_object().interface_amount,
            'architecture': self.get_object().architecture,
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_technology'] = self.post_old_data('technology', self.get_object().technology.pk)
        context['technologies'] = Technology.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina una modelo"""
    model = Model
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
