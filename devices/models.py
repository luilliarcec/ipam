from django.db import models
from racks.models import Rack


class Technology(models.Model):
    name = models.CharField(max_length=50, unique=True)
    maker = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Tecnología'
        verbose_name_plural = 'Tecnologías'
        ordering = ('name',)


class DeviceType(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Tipo de dispositivo'
        verbose_name_plural = 'Tipos de Dispositivo'
        ordering = ('name',)


class Model(models.Model):
    technology = models.ForeignKey(Technology, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    memory = models.CharField(max_length=50)
    interface_amount = models.CharField(max_length=50)
    architecture = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Modelo'
        verbose_name_plural = 'Modelos'
        ordering = ('name',)


class Device(models.Model):
    model = models.ForeignKey(Model, null=True, blank=True, on_delete=models.CASCADE)
    device_type = models.ForeignKey(DeviceType, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    serie = models.CharField(max_length=50, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Dispositivo'
        verbose_name_plural = 'Dispositivos'
        ordering = ('name',)


class DeviceDetail(models.Model):
    device = models.ForeignKey(Device, null=True, blank=True, on_delete=models.CASCADE)
    rack = models.ForeignKey(Rack, null=True, blank=True, on_delete=models.CASCADE)
    state = models.CharField(max_length=50)
    installed_at = models.DateField(default=0)
    change_at = models.DateField(default=0)
    ur_number = models.CharField(max_length=50)
    ur_amount = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.device)

    class Meta:
        verbose_name = 'Detalle de dispositivo'
        verbose_name_plural = 'Detalles de dispositivo'
        ordering = ('device',)


class DeviceInformation(models.Model):
    model = models.CharField(max_length=50)
    os_version = models.CharField(max_length=50)
    architecture = models.CharField(max_length=50)
    uptime = models.CharField(max_length=50)
    cpu = models.CharField(max_length=50)
    cpu_frequency = models.CharField(max_length=50)
    serie = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.model)

    class Meta:
        verbose_name = 'Información de dispositivo'
        verbose_name_plural = 'Información de dispositivos'
        ordering = ('model',)


class InterfazInformation(models.Model):
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    mtu = models.CharField(max_length=50)
    mac = models.CharField(max_length=50)
    ldown = models.CharField(max_length=50)
    lup = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Información de interfaz'
        verbose_name_plural = 'Información de interfaces'
        ordering = ('name',)


class IpInformation(models.Model):
    number = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    network = models.CharField(max_length=50)
    interfaz = models.CharField(max_length=50)
    dhcp = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.address)

    class Meta:
        verbose_name = 'Información de IP'
        verbose_name_plural = 'Información de IPs'
        ordering = ('address',)
