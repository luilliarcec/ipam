""" Technologies Forms """

# Django
from django import forms
# App
from devices.models import Technology


class TechnologyForm(forms.ModelForm):
    """Formulario y validacion de tecnologías"""
    name = forms.CharField(min_length=2)
    maker = forms.CharField(min_length=2)

    def clean(self):
        cleaned_data = super(TechnologyForm, self).clean()
        return cleaned_data

    class Meta:
        model = Technology
        fields = ['name', 'maker']
