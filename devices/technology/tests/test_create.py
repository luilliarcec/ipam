""" Technologies Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Technology


class CreateTechnologyTest(TestCase):
    """Prueba que se cree la Tecnologia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/device/technology/create', {
            'name': 'U',
            'maker': 'Bla Bla Bla'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Technology.objects.filter(name='U').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        Technology.objects.create(name='Ubiquiti', maker='07')

        response = self.client.post('/device/technology/create', {
            'name': 'Ubiquiti',
            'maker': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Tecnología con este Name.'
        )
        self.assertEqual(1, Technology.objects.filter(name='Ubiquiti').count())

    def test_validate_that_the_maker_is_not_less_than_2_chars(self):
        """Valida que haya un error en maker"""
        response = self.client.post('/device/technology/create', {
            'name': 'Ubiquiti',
            'maker': '0'
        })

        self.assertFormError(
            response,
            'form',
            'maker',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Technology.objects.filter(maker='0').exists())

    def test_validate_that_the_name_and_maker_are_require(self):
        """Valida que haya un error en maker"""
        Technology.objects.create(name='Ubiquiti', maker='04')

        response = self.client.post('/device/technology/create', {
            'name': '',
            'maker': ''
        })

        self.assertFormError(
            response,
            'form',
            'maker',
            'Este campo es obligatorio.'
        )

        self.assertFormError(
            response,
            'form',
            'name',
            'Este campo es obligatorio.'
        )

        self.assertEqual(1, Technology.objects.filter(name='Ubiquiti', maker='04').count())

    def test_create_a_new_technology(self):
        """Prueba la creación de una nueva provincia"""
        response = self.client.post('/device/technology/create', {
            'name': 'Ubiquiti',
            'maker': 'Tecnología de dispositivos'
        })
        self.assertTrue(Technology.objects.filter(name='Ubiquiti').exists())
