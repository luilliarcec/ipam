""" Technologies Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Technology


class DeleteTechnologyTest(TestCase):
    """Prueba que se elimine una Tecnología"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_technology(self):
        """Prueba la eliminación de una Technology"""
        model = Technology.objects.create(name='Mikrotik', maker='No sé')

        self.client.delete('/device/technology/delete/{}'.format(model.pk))

        self.assertFalse(Technology.objects.filter(name='Mikrotik').exists())