""" Technologies Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Technology


class ListTechnologyTest(TestCase):
    """Prueba la vista index de Tecnologias"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.huaweii = Technology.objects.create(
            name='Huaweii',
            maker='04',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.mikrotik = Technology.objects.create(
            name='Mikrotik',
            maker='07',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.ubiquiti = Technology.objects.create(
            name='Ubiquiti',
            maker='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_technology(self):
        """Prueba que las Tecnologias se puedan ver paginadas en el template"""
        response = self.client.get('/device/technology')
        self.assertContains(response, self.ubiquiti.name)
        self.assertContains(response, self.mikrotik.name)
        self.assertContains(response, self.huaweii.name)

    def test_that_it_can_be_filtered_by_technology_name(self):
        """Prueba que las Tecnologias se puedan filtrar por nombre"""
        response = self.client.get('/device/technology?search=huaw')
        self.assertNotContains(response, self.ubiquiti.name)
        self.assertNotContains(response, self.mikrotik.name)
        self.assertContains(response, self.huaweii.name)
