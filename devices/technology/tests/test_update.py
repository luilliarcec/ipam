""" Technologies Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import Technology


class UpdateTechnologyTest(TestCase):
    """Prueba que se actualiza la Tecnología"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.model = Technology.objects.create(name='Mikrotik', maker='No sé')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/device/technology/edit/{}'.format(self.model.pk), {
            'name': 'U',
            'maker': 'Bla Bla Bla'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Technology.objects.filter(name='Ubiquiti').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        Technology.objects.create(name='Ubiquiti', maker='07')

        response = self.client.post('/device/technology/edit/{}'.format(self.model.pk), {
            'name': 'Ubiquiti',
            'maker': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Tecnología con este Name.'
        )
        self.assertEqual(1, Technology.objects.filter(name='Ubiquiti').count())

    def test_validate_that_the_maker_is_not_less_than_2_chars(self):
        """Valida que haya un error en maker"""
        response = self.client.post('/device/technology/edit/{}'.format(self.model.pk), {
            'name': 'Ubiquiti',
            'maker': '0'
        })

        self.assertFormError(
            response,
            'form',
            'maker',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Technology.objects.filter(maker='0').exists())

    def test_validate_that_the_name_and_maker_are_require(self):
        """Valida que haya un error en maker"""
        response = self.client.post('/device/technology/edit/{}'.format(self.model.pk), {
            'name': '',
            'maker': ''
        })

        self.assertFormError(
            response,
            'form',
            'maker',
            'Este campo es obligatorio.'
        )

        self.assertFormError(
            response,
            'form',
            'name',
            'Este campo es obligatorio.'
        )

        self.assertEqual(1, Technology.objects.count())

    def test_create_a_update_technology(self):
        """Prueba la creación de una nueva provincia"""
        response = self.client.post('/device/technology/edit/{}'.format(self.model.pk), {
            'name': 'Ubiquiti',
            'maker': 'Tecnología de dispositivos'
        })
        self.assertTrue(Technology.objects.filter(name='Ubiquiti').exists())