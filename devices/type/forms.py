""" DeviceTypes Forms """

# Django
from django import forms
# App
from devices.models import DeviceType


class DeviceTypeForm(forms.ModelForm):
    """Formulario y validacion de tipos de dispositivos"""
    name = forms.CharField(min_length=2)
    description = forms.CharField(min_length=2)

    def clean(self):
        cleaned_data = super(DeviceTypeForm, self).clean()
        return cleaned_data

    class Meta:
        model = DeviceType
        fields = ['name', 'description']
