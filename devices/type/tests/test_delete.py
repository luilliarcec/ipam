""" DeviceTypes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import DeviceType


class DeleteDeviceTypeTest(TestCase):
    """Prueba que se elimine una Tipo de dispositivo"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_device_types(self):
        """Prueba la eliminación de una Tipo de dispositivo"""
        model = DeviceType.objects.create(name='Mikrotik', description='No sé')

        self.client.delete('/device/type/delete/{}'.format(model.pk))

        self.assertFalse(DeviceType.objects.filter(name='Mikrotik').exists())
