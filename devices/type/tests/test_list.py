""" DeviceTypes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import DeviceType


class ListDeviceTypeTest(TestCase):
    """Prueba la vista index de Tipos de dispositivoss"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.huaweii = DeviceType.objects.create(
            name='Huaweii',
            description='04',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.mikrotik = DeviceType.objects.create(
            name='Mikrotik',
            description='07',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.ubiquiti = DeviceType.objects.create(
            name='Ubiquiti',
            description='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_device_types(self):
        """Prueba que las Tipos de dispositivoss se puedan ver paginadas en el template"""
        response = self.client.get('/device/type')
        self.assertContains(response, self.ubiquiti.name)
        self.assertContains(response, self.mikrotik.name)
        self.assertContains(response, self.huaweii.name)

    def test_that_it_can_be_filtered_by_device_types_name(self):
        """Prueba que las Tipos de dispositivoss se puedan filtrar por nombre"""
        response = self.client.get('/device/type?search=huaw')
        self.assertNotContains(response, self.ubiquiti.name)
        self.assertNotContains(response, self.mikrotik.name)
        self.assertContains(response, self.huaweii.name)
