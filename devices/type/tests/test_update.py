""" DeviceTypes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from devices.models import DeviceType


class UpdateDeviceTypeTest(TestCase):
    """Prueba que se actualiza la Tipo de dispositivo"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.model = DeviceType.objects.create(name='Mikrotik', description='No sé')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/device/type/edit/{}'.format(self.model.pk), {
            'name': 'U',
            'description': 'Bla Bla Bla'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(DeviceType.objects.filter(name='Ubiquiti').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        DeviceType.objects.create(name='Ubiquiti', description='07')

        response = self.client.post('/device/type/edit/{}'.format(self.model.pk), {
            'name': 'Ubiquiti',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Tipo de dispositivo con este Name.'
        )
        self.assertEqual(1, DeviceType.objects.filter(name='Ubiquiti').count())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/device/type/edit/{}'.format(self.model.pk), {
            'name': 'Ubiquiti',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(DeviceType.objects.filter(description='0').exists())

    def test_validate_that_the_name_and_description_are_require(self):
        """Valida que haya un error en description"""
        response = self.client.post('/device/type/edit/{}'.format(self.model.pk), {
            'name': '',
            'description': ''
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Este campo es obligatorio.'
        )

        self.assertFormError(
            response,
            'form',
            'name',
            'Este campo es obligatorio.'
        )

        self.assertEqual(1, DeviceType.objects.count())

    def test_create_a_update_device_types(self):
        """Prueba la creación de una nueva Tipo de dispositivo"""
        response = self.client.post('/device/type/edit/{}'.format(self.model.pk), {
            'name': 'Ubiquiti',
            'description': 'Tipo de dispositivos'
        })
        self.assertTrue(DeviceType.objects.filter(name='Ubiquiti').exists())