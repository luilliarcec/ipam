""" DeviceTypes Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from devices.type.forms import DeviceTypeForm
from devices.models import DeviceType


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista las marcas"""
    template_name = 'devices/types/index.html'
    model = DeviceType
    paginate_by = 15
    context_object_name = 'types'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return DeviceType.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea una marca"""
    model = DeviceType
    template_name = 'devices/types/create.html'
    form_class = DeviceTypeForm
    success_url = reverse_lazy('devices:type.index')
    attributes = {'name': '', 'description': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza una marca"""
    model = DeviceType
    template_name = 'devices/types/edit.html'
    form_class = DeviceTypeForm
    success_url = reverse_lazy('devices:type.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'description': self.get_object().description
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina una marca"""
    model = DeviceType
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
