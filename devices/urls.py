"""Devices Urls"""

# Django
from django.urls import path

# Views
import devices.technology.views as technology
import devices.type.views as devicetype
import devices.model.views as model
import devices.device.views as device

urlpatterns = [
    path(route='technology', view=technology.Index.as_view(), name='technology.index'),
    path(route='technology/create', view=technology.Create.as_view(), name='technology.store'),
    path(route='technology/edit/<pk>', view=technology.Update.as_view(), name='technology.update'),
    path(route='technology/delete/<pk>', view=technology.Delete.as_view(), name='technology.delete'),

    path(route='type', view=devicetype.Index.as_view(), name='type.index'),
    path(route='type/create', view=devicetype.Create.as_view(), name='type.store'),
    path(route='type/edit/<pk>', view=devicetype.Update.as_view(), name='type.update'),
    path(route='type/delete/<pk>', view=devicetype.Delete.as_view(), name='type.delete'),

    path(route='model', view=model.Index.as_view(), name='model.index'),
    path(route='model/create', view=model.Create.as_view(), name='model.store'),
    path(route='model/edit/<pk>', view=model.Update.as_view(), name='model.update'),
    path(route='model/delete/<pk>', view=model.Delete.as_view(), name='model.delete'),

    path(route='', view=device.Index.as_view(), name='device.index'),
    path(route='create', view=device.Create.as_view(), name='device.store'),
    path(route='edit/<pk>', view=device.Update.as_view(), name='device.update'),
    path(route='show/<pk>', view=device.Show.as_view(), name='device.show'),
    path(route='delete/<pk>', view=device.Delete.as_view(), name='device.delete'),

    path(route='<pk>/address/add', view=device.AddAddress.as_view(), name='device.add.address'),
    path(route='<pk>/address/delete/<num>', view=device.DeleteAddress.as_view(), name='device.delete.address'),
    path(route='report', view=device.ReportDevice.as_view(), name='device.report.list'),
    path(route='report/<pk>', view=device.ReportDeviceDetail.as_view(), name='device.report.detail'),
]
