from django.contrib import admin
from locations.models import *

# Register your models here.
admin.site.register(Province)
admin.site.register(Canton)
admin.site.register(Node)
