""" Cantons Forms """

# Django
from django import forms
# App
from locations.models import Province, Canton


class CantonForm(forms.ModelForm):
    """Formulario y validacion de cantón"""
    province = forms.ModelChoiceField(required=True, queryset=Province.objects.all())
    name = forms.CharField(min_length=2, max_length=120)

    def clean(self):
        cleaned_data = super(CantonForm, self).clean()
        return cleaned_data

    class Meta:
        model = Canton
        fields = ['name', 'province']