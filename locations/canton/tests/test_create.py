""" Cantons Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Canton, Province


class CreateCantonTest(TestCase):
    """Prueba que se cree el cantón"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.guayas = Province.objects.create(name='Guayas', code='04')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/location/canton/create', {
            'name': 'G',
            'province': self.guayas.pk
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Canton.objects.filter(name='G').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        Canton.objects.create(name='Guayaquil', province=self.guayas)

        response = self.client.post('/location/canton/create', {
            'name': 'Guayaquil',
            'province': self.guayas.pk
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Cantón con este Name.'
        )

        self.assertEqual(1, Canton.objects.filter(name='Guayaquil').count())

    def test_validate_that_the_province_exists_in_database(self):
        """Valida que haya un error en canton"""
        response = self.client.post('/location/canton/create', {
            'name': 'Guayaquil',
            'province': 'abc'
        })

        self.assertFormError(
            response,
            'form',
            'province',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

        self.assertFalse(Canton.objects.filter(name='Guayaquil').exists())

    def test_validate_that_the_name_and_province_are_required(self):
        """Valida que haya un error en canton"""
        response = self.client.post('/location/canton/create', {
            'name': '',
            'province': ''
        })

        self.assertFormError(response, 'form', 'name', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'province', 'Este campo es obligatorio.')

        self.assertEqual(0, Canton.objects.count())

    def test_create_a_new_Canton(self):
        """Prueba la creación de una nueva cantón"""
        response = self.client.post('/location/canton/create', {
            'name': 'Guayaquil',
            'province': self.guayas.pk
        })

        self.assertTrue(Canton.objects.filter(name='Guayaquil').exists())

