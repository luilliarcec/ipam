""" Cantons Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Canton, Province


class DeleteCantonTest(TestCase):
    """Prueba que se elimine una cantón"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_Canton(self):
        """Prueba la eliminación de una cantón"""
        guayas = Province.objects.create(name='Guayas', code='04')

        model = Canton.objects.create(name='Guyas', province=guayas)

        response = self.client.delete('/location/canton/delete/{}'.format(model.pk))

        self.assertFalse(Canton.objects.filter(name='Guyas', province=guayas).exists())
