""" Cantons Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Canton, Province


class ListCantonTest(TestCase):
    """Prueba la vista index de Cantones"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Guayas', code='04')
        pichincha = Province.objects.create(name='Pichincha', code='07')
        playas = Province.objects.create(name='Playas', code='05')

        self.guayaquil = Canton.objects.create(
            name='Guayaquil',
            province=guayas,
            created_at=datetime.now() - timedelta(days=2)
        )

        self.milagro = Canton.objects.create(
            name='Milagro',
            province=guayas,
            created_at=datetime.now() - timedelta(days=1)
        )

        self.quito = Canton.objects.create(
            name='Quito',
            province=pichincha,
            created_at=datetime.now() + timedelta(days=1)
        )

        self.salinas = Canton.objects.create(
            name='Salinas',
            province=playas,
            created_at=datetime.now() + timedelta(days=2)
        )

    def test_that_you_can_see_list_of_cantons(self):
        """Prueba que los cantones se puedan ver paginadas en el template"""
        response = self.client.get('/location/canton')
        self.assertContains(response, self.salinas)
        self.assertContains(response, self.quito)
        self.assertContains(response, self.milagro)
        self.assertContains(response, self.guayaquil)

    def test_that_it_can_be_filtered_by_canton_name(self):
        """Prueba que las cantóns se puedan filtrar por nombre"""
        response = self.client.get('/location/canton?search=guayaq')
        self.assertNotContains(response, self.milagro)
        self.assertNotContains(response, self.salinas)
        self.assertContains(response, self.guayaquil)

    def test_that_it_can_be_filtered_by_province_name(self):
        """Prueba que las cantóns se puedan filtrar por nombre"""
        response = self.client.get('/location/canton?search=guaya')
        self.assertContains(response, self.milagro)
        self.assertContains(response, self.guayaquil)
        self.assertNotContains(response, self.salinas)
        self.assertNotContains(response, self.quito)
