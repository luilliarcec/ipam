""" Cantons Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Canton, Province


class UpdateCantonTest(TestCase):
    """Prueba que se actualize una cantón"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.guayas = Province.objects.create(name='Guayas', code='04')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        model = Canton.objects.create(name='Milagro', province=self.guayas)

        response = self.client.post('/location/canton/edit/{}'.format(model.pk), {
            'name': 'G',
            'province': self.guayas.pk
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Canton.objects.filter(name='G').exists())
        self.assertTrue(Canton.objects.filter(name='Milagro').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        model = Canton.objects.create(name='Guyas', province=self.guayas)

        Canton.objects.create(name='Milagro', province=self.guayas)

        response = self.client.post('/location/canton/edit/{}'.format(model.pk), {
            'name': 'Milagro',
            'province': self.guayas.pk
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Cantón con este Name.'
        )

        self.assertEqual(1, Canton.objects.filter(name='Milagro').count())

    def test_validate_that_the_province_exists_in_database(self):
        """Valida que haya un error en province"""
        model = Canton.objects.create(name='Guyas', province=self.guayas)

        response = self.client.post('/location/canton/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'province': '0'
        })

        self.assertFormError(
            response,
            'form',
            'province',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

        self.assertFalse(Canton.objects.filter(name='Guayas').exists())

    def test_update_only_name_Canton(self):
        """Prueba la atualización de una cantón"""
        model = Canton.objects.create(name='Guyas', province=self.guayas)

        response = self.client.post('/location/canton/edit/{}'.format(model.pk), {
            'name': 'Guayaquil',
            'province': self.guayas.pk
        })

        self.assertTrue(Canton.objects.filter(name='Guayaquil', province=self.guayas).exists())
        self.assertFalse(Canton.objects.filter(name='Guyas', province=self.guayas).exists())

    def test_update_only_province_Canton(self):
        """Prueba la atualización de una cantón"""
        playas = Province.objects.create(name='Playas', code='05')
        model = Canton.objects.create(name='Milagro', province=playas)

        response = self.client.post('/location/canton/edit/{}'.format(model.pk), {
            'name': 'Milagro',
            'province': self.guayas.pk
        })

        self.assertTrue(Canton.objects.filter(name='Milagro', province=self.guayas).exists())
        self.assertFalse(Canton.objects.filter(name='Milagro', province=playas).exists())