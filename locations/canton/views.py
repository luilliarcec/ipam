""" Canton Views """

# Django
from django.db.models import Q
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from locations.canton.forms import CantonForm
from locations.models import Canton, Province


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista los cantones"""
    template_name = 'locations/cantons/index.html'
    model = Canton
    paginate_by = 15
    context_object_name = 'cantons'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Canton.objects.filter(
            Q(name__icontains=search) |
            Q(province__name__icontains=search)
        ).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea un cantón"""
    model = Canton
    template_name = 'locations/cantons/create.html'
    form_class = CantonForm
    success_url = reverse_lazy('locations:canton.index')
    attributes = {'name': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_province'] = self.post_old_data('province')
        context['provinces'] = Province.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza un cantón"""
    model = Canton
    template_name = 'locations/cantons/edit.html'
    form_class = CantonForm
    success_url = reverse_lazy('locations:canton.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_province'] = self.post_old_data('province', self.get_object().province.pk)
        context['provinces'] = Province.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina un cantón"""
    model = Canton
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
