from django.db import models


class Province(models.Model):
    """Provincias del Ecuador"""
    name = models.CharField(max_length=120, unique=True)
    code = models.CharField(max_length=10, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Provincia'
        verbose_name_plural = 'Provincias'
        ordering = ('name',)


class Canton(models.Model):
    """Cantones que pertenecen a una provincia del Ecuador"""
    province = models.ForeignKey(Province, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=120, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Cantón'
        verbose_name_plural = 'Cantones'
        ordering = ('name',)


class Node(models.Model):
    """Nodos de eqeuipos con su locaciones"""
    canton = models.ForeignKey(Canton, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=120, unique=True)
    level = models.CharField(max_length=3)
    installed_at = models.DateField(default=0)
    address = models.CharField(max_length=120)
    coordinates = models.CharField(max_length=50)
    contact_name = models.CharField(max_length=120)
    contact_phone = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Nodo'
        verbose_name_plural = 'Nodos'
        ordering = ('name',)
