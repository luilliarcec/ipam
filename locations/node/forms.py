""" Nodes Forms """

# Django
from django import forms
from django.core import validators
# App
from locations.models import Canton, Node


class NodeForm(forms.ModelForm):
    """Formulario y validacion de nodos"""
    canton = forms.ModelChoiceField(required=True, queryset=Canton.objects.all())
    name = forms.CharField(min_length=2)
    installed_at = forms.DateField()
    contact_phone = forms.CharField(min_length=10,  max_length=20, validators=[
        validators.RegexValidator(
            regex=r'^\d+$',
            message='Solo se permiten números.',
            code='invalid_phone_number'
        ),
    ])

    def clean(self):
        cleaned_data = super(NodeForm, self).clean()
        return cleaned_data

    class Meta:
        model = Node
        fields = [
            'canton',
            'name',
            'level',
            'installed_at',
            'address',
            'coordinates',
            'contact_name',
            'contact_phone'
        ]
