""" Nodes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Canton, Node, Province


class CreateNodeTest(TestCase):
    """Prueba que se cree el nodo"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Guayas', code='04')
        self.guayaquil = Canton.objects.create(name='Guayaquil', province=guayas)

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/location/node/create', {
            'name': 'N',
            'level': 3,
            'canton': self.guayaquil.pk,
            'installed_at': datetime.now().date(),
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Node.objects.filter(name='N').exists())

    def test_validate_that_the_name_is_required(self):
        """Valida que haya un error en name"""
        response = self.client.post('/location/node/create', {
            'name': '',
            'level': 3,
            'canton': self.guayaquil.pk,
            'installed_at': datetime.now().date(),
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        Node.objects.create(
            name='Nodo Santo',
            level=3,
            canton=self.guayaquil,
            installed_at=datetime.now().date(),
            address='Por aquí',
            coordinates='0000',
            contact_name='Luis Arce',
            contact_phone='0960603626',
        )

        response = self.client.post('/location/node/create', {
            'name': 'Nodo Santo',
            'level': 3,
            'canton': self.guayaquil.pk,
            'installed_at': datetime.now().date(),
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Nodo con este Name.'
        )

        self.assertEqual(1, Node.objects.filter(name='Nodo Santo').count())

    def test_validate_that_the_level_is_required(self):
        """Valida que haya un error en level"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '',
            'canton': self.guayaquil.pk,
            'installed_at': datetime.now().date(),
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'level',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_canton_is_required(self):
        """Valida que haya un error en canton"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '',
            'installed_at': datetime.now().date(),
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'canton',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_canton_exists_in_database(self):
        """Valida que haya un error en canton"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': datetime.now().date(),
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'canton',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_date_installed_is_required(self):
        """Valida que haya un error en installed_at"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': '',
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'installed_at',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_date_installed_is_a_date_valid(self):
        """Valida que haya un error en installed_at"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': '25/ab/2018',
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'installed_at',
            'Introduzca una fecha válida.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_address_is_required(self):
        """Valida que haya un error en address"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': '25/ab/2018',
            'address': '',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'address',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_coordinates_is_required(self):
        """Valida que haya un error en coordinates"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': '25/ab/2018',
            'address': 'Por aquí',
            'coordinates': '',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'coordinates',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_contact_name_is_required(self):
        """Valida que haya un error en contact_name"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': '25/ab/2018',
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': '',
            'contact_phone': '0960603626',
        })

        self.assertFormError(
            response,
            'form',
            'contact_name',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_contact_phone_is_required(self):
        """Valida que haya un error en contact_phone"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': '25/ab/2018',
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '',
        })

        self.assertFormError(
            response,
            'form',
            'contact_phone',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_validate_that_the_contact_phone_is_number(self):
        """Valida que haya un error en contact_phone"""
        response = self.client.post('/location/node/create', {
            'name': 'Nodo San Miguel',
            'level': '3',
            'canton': '225',
            'installed_at': '25/ab/2018',
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': 'asdasd',
        })

        self.assertFormError(
            response,
            'form',
            'contact_phone',
            'Solo se permiten números.'
        )

        self.assertEqual(0, Node.objects.count())

    def test_create_a_new_node(self):
        """Prueba la creación de una nueva cantón"""
        self.client.post('/location/node/create', {
            'name': 'Nodo Santo',
            'level': 3,
            'canton': self.guayaquil.pk,
            'installed_at': datetime.now().date(),
            'address': 'Por aquí',
            'coordinates': '0000',
            'contact_name': 'Luis Arce',
            'contact_phone': '0960603626',
        })

        self.assertTrue(Node.objects.filter(name='Nodo Santo').exists())
