""" Nodes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Canton, Node, Province


class DeleteNodoTest(TestCase):
    """Prueba que se elimine un nodo"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def create_node(self, canton):
        return Node.objects.create(
            name='Nodo San Rafael',
            level=2,
            canton=canton,
            installed_at=datetime.now().date(),
            address='Por aquí',
            coordinates='0000',
            contact_name='Luis Arce',
            contact_phone='0960603626',
        )

    def test_delete_a_node(self):
        """Prueba la eliminación de una cantón"""
        guayas = Province.objects.create(name='Guayas', code='04')

        guayaquil = Canton.objects.create(name='Guayaquil', province=guayas)

        model = self.create_node(guayaquil)

        self.client.delete('/location/node/delete/{}'.format(model.pk))

        self.assertFalse(Node.objects.filter(name=model.name).exists())