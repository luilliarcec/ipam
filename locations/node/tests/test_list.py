""" Nodes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Canton, Node, Province


class ListNodeTest(TestCase):
    """Prueba la vista index de nodos"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Guayas', code='04')

        guayaquil = Canton.objects.create(name='Guayaquil', province=guayas)
        milagro = Canton.objects.create(name='Milagro', province=guayas)

        self.node_san_iguana = Node.objects.create(
            canton=guayaquil,
            name='node_san_iguana',
            level='3',
            address='por ahí',
            coordinates='0000',
            contact_name='Sper Man',
            contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )

        self.node_san_rafa = Node.objects.create(
            canton=guayaquil,
            name='node_san_rafa',
            level='1',
            address='por ahí',
            coordinates='0000',
            contact_name='Sper Man',
            contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=25),
            created_at=datetime.now() - timedelta(days=25)
        )

        self.node_san_aqui = Node.objects.create(
            canton=milagro,
            name='node_san_aqui',
            address='por ahí',
            coordinates='0000',
            contact_name='Sper Man',
            contact_phone='0000',
            installed_at=datetime.now() + timedelta(days=1),
            created_at=datetime.now() + timedelta(days=1)
        )

        self.node_san_alla = Node.objects.create(
            canton=milagro,
            name='node_san_alla',
            address='por ahí',
            coordinates='0000',
            contact_name='Sper Man',
            contact_phone='0000',
            installed_at=datetime.now() + timedelta(days=2),
            created_at=datetime.now() + timedelta(days=2)
        )

    def test_that_you_can_see_the_list_of_nodes(self):
        """Prueba que los nodos se puedan ver paginadas en el template"""
        response = self.client.get('/location/node')
        self.assertContains(response, self.node_san_alla.name)
        self.assertContains(response, self.node_san_aqui.name)
        self.assertContains(response, self.node_san_rafa.name)
        self.assertContains(response, self.node_san_iguana.name)

    def test_that_it_can_be_filtered_by_name(self):
        """Prueba que las cantóns se puedan filtrar por nombre"""
        response = self.client.get('/location/node?search=node_san_ig')
        self.assertNotContains(response, self.node_san_rafa.name)
        self.assertNotContains(response, self.node_san_alla.name)
        self.assertContains(response, self.node_san_iguana.name)

    def test_that_it_can_be_filtered_by_canton_name(self):
        """Prueba que las cantóns se puedan filtrar por nombre"""
        response = self.client.get('/location/node?search=guaya')
        self.assertContains(response, self.node_san_rafa.name)
        self.assertContains(response, self.node_san_iguana.name)
        self.assertNotContains(response, self.node_san_alla.name)
        self.assertNotContains(response, self.node_san_aqui.name)
