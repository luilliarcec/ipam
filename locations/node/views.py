""" Nodes Views """

# Django
from django.db.models import Q
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from locations.node.forms import NodeForm
from locations.models import Node, Canton


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista los nodos"""
    template_name = 'locations/nodes/index.html'
    model = Node
    paginate_by = 15
    context_object_name = 'nodes'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Node.objects.filter(
            Q(name__icontains=search) |
            Q(level__icontains=search) |
            Q(canton__name__icontains=search)
        ).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea un nodo"""
    model = Node
    template_name = 'locations/nodes/create.html'
    form_class = NodeForm
    success_url = reverse_lazy('locations:node.index')
    attributes = {
        'name': '',
        'installed_at': '',
        'level': '',
        'address': '',
        'coordinates': '',
        'contact_name': '',
        'contact_phone': '',
    }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_canton'] = self.post_old_data('canton')
        context['cantons'] = Canton.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza un nodo"""
    model = Node
    template_name = 'locations/nodes/edit.html'
    form_class = NodeForm
    success_url = reverse_lazy('locations:node.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'installed_at': self.get_object().installed_at.isoformat,
            'level': self.get_object().level,
            'address': self.get_object().address,
            'coordinates': self.get_object().coordinates,
            'contact_name': self.get_object().contact_name,
            'contact_phone': self.get_object().contact_phone,
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_canton'] = self.get_old_data('canton', self.get_object().canton.pk)
        context['cantons'] = Canton.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina un nodo"""
    model = Node
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
