""" Provinces Forms """

# Django
from django import forms
from django.core import validators
# App
from locations.models import Province


class ProvinceForm(forms.ModelForm):
    """Formulario y validacion de provincia"""
    name = forms.CharField(min_length=2, max_length=120)
    code = forms.CharField(min_length=2, max_length=10, validators=[
        validators.RegexValidator(
            regex=r'^\d+$',
            message='Solo se permiten números.',
            code='invalid_phone_number'
        ),
    ])

    def clean(self):
        cleaned_data = super(ProvinceForm, self).clean()
        return cleaned_data

    class Meta:
        model = Province
        fields = ['name', 'code']
