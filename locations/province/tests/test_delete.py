""" Provinces Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Province


class DeleteProvinceTest(TestCase):
    """Prueba que se elimine una provincia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_province(self):
        """Prueba la eliminación de una provincia"""
        model = Province.objects.create(name='Guyas', code='27')

        response = self.client.delete('/location/province/delete/{}'.format(model.pk))

        self.assertFalse(Province.objects.filter(name='Guyas', code='27').exists())