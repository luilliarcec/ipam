""" Provinces Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Province


class ListProvinceTest(TestCase):
    """Prueba la vista index de Provincias"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.guayas = Province.objects.create(
            name='Guayas',
            code='04',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.pichincha = Province.objects.create(
            name='Pichincha',
            code='07',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.playas = Province.objects.create(
            name='Playas',
            code='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_provinces_order_by_desc_created_at(self):
        """Prueba que las provincias se puedan ver paginadas en el template"""
        response = self.client.get('/location/province')
        self.assertContains(response, self.playas)
        self.assertContains(response, self.pichincha)
        self.assertContains(response, self.guayas)

    def test_that_it_can_be_filtered_by_province_name(self):
        """Prueba que las provincias se puedan filtrar por nombre"""
        response = self.client.get('/location/province?search=guay')
        self.assertNotContains(response, self.playas)
        self.assertNotContains(response, self.pichincha)
        self.assertContains(response, self.guayas)
