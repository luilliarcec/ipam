""" Provinces Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from locations.models import Province


class UpdateProvinceTest(TestCase):
    """Prueba que se actualize una provincia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        model = Province.objects.create(name='Guyas', code='27')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'G',
            'code': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Province.objects.filter(name='G').exists())
        self.assertTrue(Province.objects.filter(name='Guyas').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        model = Province.objects.create(name='Guyas', code='27')
        Province.objects.create(name='Pichincha', code='07')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Pichincha',
            'code': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Provincia con este Name.'
        )
        self.assertTrue(Province.objects.filter(name='Pichincha', code='07').exists())
        self.assertFalse(Province.objects.filter(name='Pichincha', code='04').exists())

    def test_validate_that_the_code_is_not_less_than_2_chars(self):
        """Valida que haya un error en code"""
        model = Province.objects.create(name='Guyas', code='27')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'code': '0'
        })

        self.assertFormError(
            response,
            'form',
            'code',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Province.objects.filter(code='0').exists())

    def test_validate_that_the_code_is_not_greater_than_10_chars(self):
        """Valida que haya un error en code"""
        model = Province.objects.create(name='Guyas', code='27')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'code': '01234567890'
        })

        self.assertFormError(
            response,
            'form',
            'code',
            'Asegúrese de que este valor tenga menos de 10 caracteres (tiene 11).'
        )
        self.assertFalse(Province.objects.filter(code='01234567890').exists())

    def test_validate_that_the_code_is_only_digits(self):
        """Valida que haya un error en code"""
        model = Province.objects.create(name='Guyas', code='27')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'code': 'abcd0123'
        })

        self.assertFormError(
            response,
            'form',
            'code',
            'Solo se permiten números.'
        )
        self.assertFalse(Province.objects.filter(code='abcd0123').exists())

    def test_validate_that_the_code_is_unique(self):
        """Valida que haya un error en code"""
        model = Province.objects.create(name='Guyas', code='27')
        Province.objects.create(name='Pichincha', code='04')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'code': '04'
        })

        self.assertFormError(
            response,
            'form',
            'code',
            'Ya existe Provincia con este Code.'
        )
        self.assertEqual(1, Province.objects.filter(code='04').count())
        self.assertTrue(Province.objects.filter(name='Guyas').exists())

    def test_update_only_name_province(self):
        """Prueba la atualización de una provincia"""
        model = Province.objects.create(name='Guyas', code='04')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'code': '04'
        })

        self.assertTrue(Province.objects.filter(name='Guayas', code='04').exists())
        self.assertFalse(Province.objects.filter(name='Guyas', code='04').exists())

    def test_update_only_code_province(self):
        """Prueba la atualización de una provincia"""
        model = Province.objects.create(name='Guayas', code='12')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'code': '04'
        })

        self.assertTrue(Province.objects.filter(name='Guayas', code='04').exists())
        self.assertFalse(Province.objects.filter(name='Guayas', code='12').exists())

    def test_update_a_province(self):
        """Prueba la atualización de una provincia"""
        model = Province.objects.create(name='Guyas', code='27')

        response = self.client.post('/location/province/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'code': '04'
        })

        self.assertTrue(Province.objects.filter(name='Guayas', code='04').exists())
        self.assertFalse(Province.objects.filter(name='Guyas', code='27').exists())