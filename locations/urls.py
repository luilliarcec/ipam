"""Locations Urls"""

# Django
from django.urls import path

# Views
import locations.canton.views as canton
import locations.province.views as province
import locations.node.views as node

urlpatterns = [
    path(route='province', view=province.Index.as_view(), name='province.index'),
    path(route='province/create', view=province.Create.as_view(), name='province.store'),
    path(route='province/edit/<pk>', view=province.Update.as_view(), name='province.update'),
    path(route='province/delete/<pk>', view=province.Delete.as_view(), name='province.delete'),

    path(route='canton', view=canton.Index.as_view(), name='canton.index'),
    path(route='canton/create', view=canton.Create.as_view(), name='canton.store'),
    path(route='canton/edit/<pk>', view=canton.Update.as_view(), name='canton.update'),
    path(route='canton/delete/<pk>', view=canton.Delete.as_view(), name='canton.delete'),

    path(route='node', view=node.Index.as_view(), name='node.index'),
    path(route='node/create', view=node.Create.as_view(), name='node.store'),
    path(route='node/edit/<pk>', view=node.Update.as_view(), name='node.update'),
    path(route='node/delete/<pk>', view=node.Delete.as_view(), name='node.delete'),
]
