""" IP Address Forms """

# Django
from django import forms
# App
from networks.models import IpAddress, Subnet, Device


class IpAddressForm(forms.ModelForm):
    """Formulario y validacion de vlan"""
    address = forms.GenericIPAddressField()
    state = forms.CharField(min_length=2)
    subnet = forms.ModelChoiceField(required=True, queryset=Subnet.objects.all())
    device = forms.ModelChoiceField(required=True, queryset=Device.objects.all())

    def clean(self):
        cleaned_data = super(IpAddressForm, self).clean()
        return cleaned_data

    class Meta:
        model = IpAddress
        fields = ['state', 'address', 'subnet', 'device']
