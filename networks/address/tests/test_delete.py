""" IP Address Delete Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from networks.models import Subnet, SubnetGroup, IpAddress
from devices.models import Device, DeviceType, Technology, Model


class DeleteIpAddressTest(TestCase):
    """Prueba que se elimine una IpAddress"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_vlan(self):
        """Prueba la eliminación de una IpAddress"""
        group = SubnetGroup.objects.create(name='Grp Faker', description='Grb Desc')

        device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )

        subnet = Subnet.objects.create(
            subnet='Sub Faker', description='Subnet Desc', state='Active', subnet_group=group
        )

        device = Device.objects.create(name='Device Faker', device_type=device_type, model=model, serie=4005)

        self.model = IpAddress.objects.create(
            address='192.168.10.1', state='04', subnet=subnet, device=device
        )

        self.client.delete('/network/address/delete/{}'.format(self.model.pk))

        self.assertFalse(IpAddress.objects.filter(address='192.168.10.1').exists())
