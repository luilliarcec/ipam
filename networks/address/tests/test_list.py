""" IP Address List Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Vlan, VlanGroup, IpAddress, Subnet, SubnetGroup
from devices.models import Device, DeviceType, Technology, Model


class ListIpAddressTest(TestCase):
    """Prueba la vista index de IpAddress"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        group = SubnetGroup.objects.create(name='Grp Faker', description='Grb Desc')

        device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )
        
        subnet = Subnet.objects.create(
            subnet='Sub Faker', description='Subnet Desc', state='Active', subnet_group=group
        )

        device = Device.objects.create(name='Device Faker', device_type=device_type, model=model, serie=4005)
        device1 = Device.objects.create(name='Device Faker 1', device_type=device_type, model=model, serie=1024)
        device2 = Device.objects.create(name='Device Faker 2', device_type=device_type, model=model, serie=2048)

        self.address_1 = IpAddress.objects.create(
            address='192.168.10.1',
            state='04',
            subnet=subnet,
            device=device,
            created_at=datetime.now() - timedelta(days=2)
        )
        self.address_2 = IpAddress.objects.create(
            address='110.168.10.1',
            state='07',
            subnet=subnet,
            device=device1,
            created_at=datetime.now() - timedelta(days=1)
        )

        self.address_3 = IpAddress.objects.create(
            address='220.168.30.40',
            state='05',
            subnet=subnet,
            device=device2,
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_addresss(self):
        """Prueba que las IpAddress se puedan ver paginadas en el template"""
        response = self.client.get('/network/address?page=1')
        self.assertContains(response, self.address_3.address)
        self.assertContains(response, self.address_2.address)
        self.assertContains(response, self.address_1.address)

    def test_that_it_can_be_filtered_by_address_name(self):
        """Prueba que las IpAddress se puedan filtrar por nombre"""
        response = self.client.get('/network/address?search=192.1')
        self.assertNotContains(response, self.address_3.address)
        self.assertNotContains(response, self.address_2.address)
        self.assertContains(response, self.address_1.address)
