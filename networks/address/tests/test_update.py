""" IP Address Update Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from networks.models import Subnet, SubnetGroup, IpAddress
from devices.models import Device, DeviceType, Technology, Model


class UpdateVlanTest(TestCase):
    """Prueba que se actualize una vlan"""
    
    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        group = SubnetGroup.objects.create(name='Grp Faker', description='Grb Desc')

        device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )

        self.subnet = Subnet.objects.create(
            subnet='Sub Faker', description='Subnet Desc', state='Active', subnet_group=group
        )
        self.device = Device.objects.create(name='Device Faker 125', device_type=device_type, model=model, serie=4005)
        
        self.model = IpAddress.objects.create(
            address='192.168.10.1', state='04', subnet=self.subnet, device=self.device
        )
    
    def test_validate_that_the_state_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/network/address/edit/{}'.format(self.model.pk), {
            'state': 'A',
        })

        self.assertFormError(
            response,
            'form',
            'state',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(IpAddress.objects.filter(state='A').exists())

    def test_validate_that_the_address_is_a_ip_valid(self):
        """Valida que haya un error en name"""
        response = self.client.post('/network/address/edit/{}'.format(self.model.pk), {
            'address': '192,68,65',
        })

        self.assertFormError(
            response,
            'form',
            'address',
            'Introduzca una dirección IPv4 o IPv6 válida.'
        )
        self.assertFalse(IpAddress.objects.filter(address='192,68,65').exists())

    def test_create_a_new_address(self):
        """Prueba la creación de una nueva IpAddress"""
        self.client.post('/network/address/edit/{}'.format(self.model.pk), {
            'address': '192.168.120.1',
            'state': 'Activo',
            'subnet': self.subnet.pk,
            'device': self.device.pk
        })
        self.assertTrue(IpAddress.objects.filter(address='192.168.120.1').exists())
