""" IP Address Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from networks.address.forms import IpAddressForm
from networks.models import IpAddress, Subnet
from devices.models import Device


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista las IpAddresses"""
    template_name = 'networks/addresses/index.html'
    model = IpAddress
    paginate_by = 15
    context_object_name = 'addresses'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return IpAddress.objects.filter(address__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea una IpAddress"""
    model = IpAddress
    template_name = 'networks/addresses/create.html'
    form_class = IpAddressForm
    success_url = reverse_lazy('networks:address.index')
    attributes = {'address': '', 'state': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_subnet'] = self.post_old_data('old_subnet')
        context['subnets'] = Subnet.objects.all()
        context['old_device'] = self.post_old_data('device')
        context['devices'] = Device.objects.exclude(ipaddress__in=IpAddress.objects.all())
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza una IpAddress"""
    model = IpAddress
    template_name = 'networks/addresses/edit.html'
    form_class = IpAddressForm
    success_url = reverse_lazy('networks:address.index')

    def get_attributes(self):
        return {
            'address': self.get_object().address,
            'state': self.get_object().state,
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_subnet'] = self.post_old_data('old_subnet', self.get_object().subnet.pk)
        context['subnets'] = Subnet.objects.all()
        context['old_device'] = self.post_old_data('device', self.get_object().device.pk)
        context['devices'] = Device.objects.exclude(ipaddress__in=IpAddress.objects.exclude(address=self.get_object().address))
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina una IpAddress"""
    model = IpAddress
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
