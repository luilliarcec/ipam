from django.contrib import admin
from networks.models import *

# Register your models here.
admin.site.register(VlanGroup)
admin.site.register(Vlan)
admin.site.register(SubnetGroup)
admin.site.register(Subnet)
admin.site.register(IpAddress)
