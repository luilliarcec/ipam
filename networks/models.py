from django.db import models
from devices.models import Device


class VlanGroup(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Grupo vlan'
        verbose_name_plural = 'Grupo vlans'
        ordering = ('name',)


class Vlan(models.Model):
    vlan_group = models.ForeignKey(VlanGroup, null=True, blank=True, on_delete=models.CASCADE)
    device = models.ForeignKey(Device, null=True, blank=True, on_delete=models.CASCADE)
    vlan_id = models.CharField(max_length=4)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Vlan'
        verbose_name_plural = 'Vlans'
        ordering = ('name',)


class SubnetGroup(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Grupo subnet'
        verbose_name_plural = 'Grupo Subnets'
        ordering = ('name',)


class Subnet(models.Model):
    subnet_group = models.ForeignKey(SubnetGroup, null=True, blank=True, on_delete=models.CASCADE)
    subnet = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.subnet)

    class Meta:
        verbose_name = 'Subnet'
        verbose_name_plural = 'Subnets'
        ordering = ('subnet',)


class IpAddress(models.Model):
    subnet = models.ForeignKey(Subnet, null=True, blank=True, on_delete=models.CASCADE)
    device = models.OneToOneField(Device, on_delete=models.CASCADE)
    address = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.address)

    class Meta:
        verbose_name = 'Dirección IP'
        verbose_name_plural = 'Direcciones IPs'
        ordering = ('address',)
