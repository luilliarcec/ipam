""" Subnet Forms """

# Django
from django import forms
# App
from networks.models import Subnet, SubnetGroup


class SubnetForm(forms.ModelForm):
    """Formulario y validacion de subnet"""
    subnet = forms.CharField(min_length=2)
    description = forms.CharField(min_length=2)
    state = forms.CharField(min_length=2)
    subnet_group = forms.ModelChoiceField(required=True, queryset=SubnetGroup.objects.all())

    def clean(self):
        cleaned_data = super(SubnetForm, self).clean()
        return cleaned_data

    class Meta:
        model = Subnet
        fields = ['subnet', 'description', 'subnet_group', 'state']
