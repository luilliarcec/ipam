""" Subnet Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Subnet, SubnetGroup


class CreateSubnetTest(TestCase):
    """Prueba que se cree la provincia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_validate_that_the_subnet_is_not_less_than_2_chars(self):
        """Valida que haya un error en subnet"""
        response = self.client.post('/network/subnet/create', {
            'subnet': 'G',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'subnet',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Subnet.objects.filter(subnet='G').exists())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/subnet/create', {
            'subnet': 'Guayas',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Subnet.objects.filter(description='0').exists())

    def test_validate_that_the_state_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/subnet/create', {
            'subnet': 'Guayas',
            'description': 'Yala',
            'state': 'A',
        })

        self.assertFormError(
            response,
            'form',
            'state',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Subnet.objects.filter(state='A').exists())

    def test_validate_that_the_subnet_group_exists_in_database(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/subnet/create', {
            'subnet': 'Guayas',
            'description': 'Yala',
            'state': 'Active',
            'subnet_group': 300
        })

        self.assertFormError(
            response,
            'form',
            'subnet_group',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )
        self.assertFalse(Subnet.objects.filter(subnet='Guayas').exists())

    def test_create_a_new_subnet(self):
        """Prueba la creación de una nueva provincia"""
        group = SubnetGroup.objects.create(name='One Group', description='Desc Group')

        self.client.post('/network/subnet/create', {
            'subnet': 'Guayas',
            'description': 'Yala',
            'state': 'Active',
            'subnet_group': group.pk
        })
        self.assertTrue(Subnet.objects.filter(subnet='Guayas').exists())
