""" Subnet Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Subnet, SubnetGroup


class DeleteSubnetTest(TestCase):
    """Prueba que se elimine una provincia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_subnet(self):
        """Prueba la eliminación de una provincia"""
        group = SubnetGroup.objects.create(name='One Group', description='Desc Group')
        model = Subnet.objects.create(subnet='Faker', description='Desc', state='Active', subnet_group=group)

        self.client.delete('/network/subnet/delete/{}'.format(model.pk))

        self.assertFalse(Subnet.objects.filter(subnet='Faker').exists())
