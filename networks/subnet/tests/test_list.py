""" Subnet Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Subnet, SubnetGroup


class ListSubnetTest(TestCase):
    """Prueba la vista index de Subnet"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.subnet_1 = Subnet.objects.create(
            subnet='Guayas',
            description='04',
            state='Active',
            created_at=datetime.now() - timedelta(days=2)
        )
        self.subnet_2 = Subnet.objects.create(
            subnet='Pichincha',
            description='07',
            state='Active',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.subnet_3 = Subnet.objects.create(
            subnet='Playas',
            state='Active',
            description='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_subnets(self):
        """Prueba que las Subnet se puedan ver paginadas en el template"""
        response = self.client.get('/network/subnet')
        self.assertContains(response, self.subnet_3.subnet)
        self.assertContains(response, self.subnet_2.subnet)
        self.assertContains(response, self.subnet_1.subnet)

    def test_that_it_can_be_filtered_by_subnet_subnet(self):
        """Prueba que las Subnet se puedan filtrar por nombre"""
        response = self.client.get('/network/subnet?search=guay')
        self.assertNotContains(response, self.subnet_3.subnet)
        self.assertNotContains(response, self.subnet_2.subnet)
        self.assertContains(response, self.subnet_1.subnet)
