""" Subnet Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Subnet, SubnetGroup


class UpdateSubnetTest(TestCase):
    """Prueba que se actualize una provincia"""
    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.group = SubnetGroup.objects.create(name='One Group', description='Desc Group')
        self.model = Subnet.objects.create(subnet='Faker', description='Desc', state='Active', subnet_group=self.group)

    def test_validate_that_the_subnet_is_not_less_than_2_chars(self):
        """Valida que haya un error en subnet"""
        response = self.client.post('/network/subnet/edit/{}'.format(self.model.pk), {
            'subnet': 'G',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'subnet',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Subnet.objects.filter(subnet='G').exists())
        self.assertTrue(Subnet.objects.filter(subnet='Faker').exists())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/subnet/edit/{}'.format(self.model.pk), {
            'subnet': 'Guayas',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Subnet.objects.filter(description='0').exists())

    def test_update_a_subnet(self):
        """Prueba la atualización de una provincia"""
        self.client.post('/network/subnet/edit/{}'.format(self.model.pk), {
            'subnet': 'Guayas',
            'description': 'Yala',
            'state': 'Active',
            'subnet_group': self.group.pk
        })

        self.assertTrue(Subnet.objects.filter(subnet='Guayas').exists())
        self.assertFalse(Subnet.objects.filter(subnet='Faker').exists())
