""" Subnet Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from networks.subnet.forms import SubnetForm
from networks.models import Subnet, SubnetGroup


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista las Subnet"""
    template_name = 'networks/subnets/index.html'
    model = Subnet
    paginate_by = 15
    context_object_name = 'subnets'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Subnet.objects.filter(subnet__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea una Subnet"""
    model = Subnet
    template_name = 'networks/subnets/create.html'
    form_class = SubnetForm
    success_url = reverse_lazy('networks:subnet.index')
    attributes = {'subnet': '', 'description': '', 'state': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_subnet_group'] = self.post_old_data('old_subnet_group')
        context['subnet_groups'] = SubnetGroup.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza una Subnet"""
    model = Subnet
    template_name = 'networks/subnets/edit.html'
    form_class = SubnetForm
    success_url = reverse_lazy('networks:subnet.index')

    def get_attributes(self):
        return {
            'subnet': self.get_object().subnet,
            'description': self.get_object().description,
            'state': self.get_object().state,
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_subnet_group'] = self.post_old_data('old_subnet_group', self.get_object().subnet_group.pk)
        context['subnet_groups'] = SubnetGroup.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina una Subnet"""
    model = Subnet
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
