""" Subnet Group Forms """

# Django
from django import forms
# App
from networks.models import SubnetGroup


class SubnetGroupForm(forms.ModelForm):
    """Formulario y validacion de grupo de subnet"""
    name = forms.CharField(min_length=2)
    description = forms.CharField(min_length=2)

    def clean(self):
        cleaned_data = super(SubnetGroupForm, self).clean()
        return cleaned_data

    class Meta:
        model = SubnetGroup
        fields = ['name', 'description']
