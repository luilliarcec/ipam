""" Subnet Group Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import SubnetGroup


class DeleteSubnetGroupTest(TestCase):
    """Prueba que se elimine una provincia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_subnet_group(self):
        """Prueba la eliminación de una provincia"""
        model = SubnetGroup.objects.create(name='Guyas', description='27')

        self.client.delete('/network/subnet-group/delete/{}'.format(model.pk))

        self.assertFalse(SubnetGroup.objects.filter(name='Guyas', description='27').exists())