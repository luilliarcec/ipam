""" Subnet Group Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import SubnetGroup


class ListSubnetGroupTest(TestCase):
    """Prueba la vista index de Subnet Group"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.subnet_1 = SubnetGroup.objects.create(
            name='Guayas',
            description='04',
            created_at=datetime.now() - timedelta(days=2)
        )
        self.subnet_2 = SubnetGroup.objects.create(
            name='Pichincha',
            description='07',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.subnet_3 = SubnetGroup.objects.create(
            name='Playas',
            description='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_subnet_groups(self):
        """Prueba que las Subnet Group se puedan ver paginadas en el template"""
        response = self.client.get('/network/subnet-group')
        self.assertContains(response, self.subnet_3.name)
        self.assertContains(response, self.subnet_2.name)
        self.assertContains(response, self.subnet_1.name)

    def test_that_it_can_be_filtered_by_subnet_group_name(self):
        """Prueba que las Subnet Group se puedan filtrar por nombre"""
        response = self.client.get('/network/subnet-group?search=guay')
        self.assertNotContains(response, self.subnet_3.name)
        self.assertNotContains(response, self.subnet_2.name)
        self.assertContains(response, self.subnet_1.name)
