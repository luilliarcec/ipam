""" Subnet Group Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from networks.subnet_group.forms import SubnetGroupForm
from networks.models import SubnetGroup


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista las Subnet Groups"""
    template_name = 'networks/subnetgroups/index.html'
    model = SubnetGroup
    paginate_by = 15
    context_object_name = 'groups'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return SubnetGroup.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea una Subnet Group"""
    model = SubnetGroup
    template_name = 'networks/subnetgroups/create.html'
    form_class = SubnetGroupForm
    success_url = reverse_lazy('networks:subnet_group.index')
    attributes = {'name': '', 'description': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza una Subnet Group"""
    model = SubnetGroup
    template_name = 'networks/subnetgroups/edit.html'
    form_class = SubnetGroupForm
    success_url = reverse_lazy('networks:subnet_group.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'description': self.get_object().description
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina una Subnet Group"""
    model = SubnetGroup
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
