"""Networks Urls"""

# Django
from django.urls import path

# Views
import networks.subnet_group.views as subnet_group
import networks.vlan_group.views as vlan_group
import networks.vlan.views as vlan
import networks.subnet.views as subnet
import networks.address.views as address

urlpatterns = [
    path(route='subnet-group', view=subnet_group.Index.as_view(), name='subnet_group.index'),
    path(route='subnet-group/create', view=subnet_group.Create.as_view(), name='subnet_group.store'),
    path(route='subnet-group/edit/<pk>', view=subnet_group.Update.as_view(), name='subnet_group.update'),
    path(route='subnet-group/delete/<pk>', view=subnet_group.Delete.as_view(), name='subnet_group.delete'),

    path(route='vlan-group', view=vlan_group.Index.as_view(), name='vlan_group.index'),
    path(route='vlan-group/create', view=vlan_group.Create.as_view(), name='vlan_group.store'),
    path(route='vlan-group/edit/<pk>', view=vlan_group.Update.as_view(), name='vlan_group.update'),
    path(route='vlan-group/delete/<pk>', view=vlan_group.Delete.as_view(), name='vlan_group.delete'),

    path(route='vlan', view=vlan.Index.as_view(), name='vlan.index'),
    path(route='vlan/create', view=vlan.Create.as_view(), name='vlan.store'),
    path(route='vlan/edit/<pk>', view=vlan.Update.as_view(), name='vlan.update'),
    path(route='vlan/delete/<pk>', view=vlan.Delete.as_view(), name='vlan.delete'),

    path(route='subnet', view=subnet.Index.as_view(), name='subnet.index'),
    path(route='subnet/create', view=subnet.Create.as_view(), name='subnet.store'),
    path(route='subnet/edit/<pk>', view=subnet.Update.as_view(), name='subnet.update'),
    path(route='subnet/delete/<pk>', view=subnet.Delete.as_view(), name='subnet.delete'),

    path(route='address', view=address.Index.as_view(), name='address.index'),
    path(route='address/create', view=address.Create.as_view(), name='address.store'),
    path(route='address/edit/<pk>', view=address.Update.as_view(), name='address.update'),
    path(route='address/delete/<pk>', view=address.Delete.as_view(), name='address.delete'),
]
