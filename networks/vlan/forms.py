""" Vlan Forms """

# Django
from django import forms
from django.core import validators
# App
from networks.models import Vlan, VlanGroup, Device


class VlanForm(forms.ModelForm):
    """Formulario y validacion de vlan"""
    vlan_id = forms.CharField(min_length=4, max_length=4, validators=[
        validators.RegexValidator(
            regex=r'^\d+$',
            message='Solo se permiten números.',
            code='invalid_phone_number'
        ),
    ])
    name = forms.CharField(min_length=2)
    description = forms.CharField(min_length=2)
    device = forms.ModelChoiceField(required=True, queryset=Device.objects.all())
    vlan_group = forms.ModelChoiceField(required=True, queryset=VlanGroup.objects.all())

    def clean(self):
        cleaned_data = super(VlanForm, self).clean()
        return cleaned_data

    class Meta:
        model = Vlan
        fields = ['name', 'vlan_id', 'vlan_group', 'description', 'device']
