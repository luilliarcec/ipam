""" Vlan Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Vlan, VlanGroup
from devices.models import Device, DeviceType, Technology, Model


class CreateVlanTest(TestCase):
    """Prueba que se cree la vlan"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/network/vlan/create', {
            'name': 'G',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Vlan.objects.filter(name='G').exists())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/vlan/create', {
            'name': 'Guayas',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Vlan.objects.filter(description='0').exists())

    def test_validate_that_the_vlan_id_is_integer(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/vlan/create', {
            'name': 'Guayas',
            'description': 'Papapa',
            'vlan_id': '4BC0'
        })

        self.assertFormError(response, 'form', 'vlan_id', 'Solo se permiten números.')
        self.assertFalse(Vlan.objects.filter(name='Guayas').exists())

    def test_validate_that_the_vlan_group_and_device_exists_in_database(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/vlan/create', {
            'name': 'Guayas',
            'description': 'Papapa',
            'device': 300,
            'vlan_group': 300,
        })

        self.assertFormError(
            response,
            'form',
            'device',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )
        self.assertFormError(
            response,
            'form',
            'vlan_group',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )
        self.assertFalse(Vlan.objects.filter(name='Guayas').exists())

    def test_create_a_new_vlan(self):
        """Prueba la creación de una nueva vlan"""
        device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )
        vlan_group = VlanGroup.objects.create(name='Vlan Group Faker', description='04')
        device = Device.objects.create(name='Device Faker', device_type=device_type, model=model, serie=4005)

        self.client.post('/network/vlan/create', {
            'vlan_id': 2525,
            'name': 'Guayas',
            'description': 'Vlan Faker',
            'vlan_group': vlan_group.pk,
            'device': device.pk
        })
        self.assertTrue(Vlan.objects.filter(name='Guayas').exists())

