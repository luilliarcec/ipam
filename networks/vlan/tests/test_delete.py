""" Vlan Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Vlan, VlanGroup
from devices.models import Device, DeviceType, Technology, Model


class DeleteVlanTest(TestCase):
    """Prueba que se elimine una vlan"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_vlan(self):
        """Prueba la eliminación de una vlan"""
        device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        dmodel = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )
        vlan_group = VlanGroup.objects.create(name='Vlan Group Faker', description='04')
        device = Device.objects.create(name='Device Faker', device_type=device_type, model=dmodel, serie=4005)
        model = Vlan.objects.create(
            vlan_id=2525, name='Vlan Faker', description='04', vlan_group=vlan_group, device=device,
        )

        self.client.delete('/network/vlan/delete/{}'.format(model.pk))

        self.assertFalse(Vlan.objects.filter(name='Vlan Faker', description='27').exists())
