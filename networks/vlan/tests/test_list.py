""" Vlan Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Vlan, VlanGroup
from devices.models import Device, DeviceType, Technology, Model


class ListVlanTest(TestCase):
    """Prueba la vista index de Vlan"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        vlan_group = VlanGroup.objects.create(name='Guayas', description='04')

        self.vlan_1 = Vlan.objects.create(
            vlan_id=2525,
            name='Guayas',
            description='04',
            vlan_group=vlan_group,
            created_at=datetime.now() - timedelta(days=2)
        )
        self.vlan_2 = Vlan.objects.create(
            vlan_id=2525,
            name='Pichincha',
            description='07',
            vlan_group=vlan_group,
            created_at=datetime.now() - timedelta(days=1)
        )

        self.vlan_3 = Vlan.objects.create(
            vlan_id=2525,
            name='Playas',
            description='05',
            vlan_group=vlan_group,
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_vlans(self):
        """Prueba que las Vlan se puedan ver paginadas en el template"""
        response = self.client.get('/network/vlan?page=1')
        self.assertContains(response, self.vlan_3.name)
        self.assertContains(response, self.vlan_2.name)
        self.assertContains(response, self.vlan_1.name)

    def test_that_it_can_be_filtered_by_vlan_name(self):
        """Prueba que las Vlan se puedan filtrar por nombre"""
        response = self.client.get('/network/vlan?search=guay')
        self.assertNotContains(response, self.vlan_3.name)
        self.assertNotContains(response, self.vlan_2.name)
        self.assertContains(response, self.vlan_1.name)
