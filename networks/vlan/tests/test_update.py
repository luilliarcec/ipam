""" Vlan Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import Vlan, VlanGroup
from devices.models import Device, DeviceType, Technology, Model


class UpdateVlanTest(TestCase):
    """Prueba que se actualize una vlan"""
    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        device_type = DeviceType.objects.create(name='Huaweii', description='04')
        tech = Technology.objects.create(name='5 GHz', maker='Huaweii')
        model = Model.objects.create(
            technology=tech, name='Modelo Faker', memory=1111,
            interface_amount=11, architecture='Arq Faker',
        )
        self.vlan_group = VlanGroup.objects.create(name='Vlan Group Faker', description='04')
        self.device = Device.objects.create(name='Device Faker', device_type=device_type, model=model, serie=4005)
        self.model = Vlan.objects.create(
            vlan_id=2525, name='Vlan Faker', description='04', vlan_group=self.vlan_group, device=self.device,
        )

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/network/vlan/edit/{}'.format(self.model.pk), {
            'name': 'G',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Vlan.objects.filter(name='G').exists())
        self.assertTrue(Vlan.objects.filter(name='Vlan Faker').exists())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/vlan/edit/{}'.format(self.model.pk), {
            'name': 'Vlan Faker',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Vlan.objects.filter(description='0').exists())

    def test_update_a_vlan(self):
        """Prueba la atualización de una vlan"""
        self.client.post('/network/vlan/edit/{}'.format(self.model.pk), {
            'vlan_id': 2525,
            'name': 'Vlan Noll',
            'description': 'Vlan desc',
            'vlan_group': self.vlan_group.pk,
            'device': self.device.pk
        })

        self.assertTrue(Vlan.objects.filter(name='Vlan Noll', description='Vlan desc').exists())
        self.assertFalse(Vlan.objects.filter(name='Vlan Faker').exists())
