""" Vlan Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from networks.vlan.forms import VlanForm
from networks.models import Vlan, VlanGroup
from devices.models import Device


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista las Vlans"""
    template_name = 'networks/vlans/index.html'
    model = Vlan
    paginate_by = 15
    context_object_name = 'vlans'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Vlan.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea una Vlan"""
    model = Vlan
    template_name = 'networks/vlans/create.html'
    form_class = VlanForm
    success_url = reverse_lazy('networks:vlan.index')
    attributes = {'vlan_id': '', 'name': '', 'description': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_vlan_group'] = self.post_old_data('old_vlan_group')
        context['vlan_groups'] = VlanGroup.objects.all()
        context['old_device'] = self.post_old_data('device')
        context['devices'] = Device.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza una Vlan"""
    model = Vlan
    template_name = 'networks/vlans/edit.html'
    form_class = VlanForm
    success_url = reverse_lazy('networks:vlan.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'description': self.get_object().description,
            'vlan_id': self.get_object().vlan_id
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_vlan_group'] = self.post_old_data('old_vlan_group', self.get_object().vlan_group.pk)
        context['vlan_groups'] = VlanGroup.objects.all()
        context['old_device'] = self.post_old_data('device', self.get_object().device.pk)
        context['devices'] = Device.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina una Vlan"""
    model = Vlan
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
