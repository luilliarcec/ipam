""" Vlan Group Forms """

# Django
from django import forms
# App
from networks.models import VlanGroup


class VlanGroupForm(forms.ModelForm):
    """Formulario y validacion de grupo de subnet"""
    name = forms.CharField(min_length=2)
    description = forms.CharField(min_length=2)

    def clean(self):
        cleaned_data = super(VlanGroupForm, self).clean()
        return cleaned_data

    class Meta:
        model = VlanGroup
        fields = ['name', 'description']
