""" Vlan Group Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import VlanGroup


class CreateVlanGroupTest(TestCase):
    """Prueba que se cree la provincia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/network/vlan-group/create', {
            'name': 'G',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(VlanGroup.objects.filter(name='G').exists())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/network/vlan-group/create', {
            'name': 'Guayas',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(VlanGroup.objects.filter(description='0').exists())

    def test_create_a_new_vlan_group(self):
        """Prueba la creación de una nueva provincia"""
        self.client.post('/network/vlan-group/create', {
            'name': 'Guayas',
            'description': '04'
        })
        self.assertTrue(VlanGroup.objects.filter(name='Guayas').exists())

