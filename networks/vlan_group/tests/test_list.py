""" Vlan Group Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import VlanGroup


class ListVlanGroupTest(TestCase):
    """Prueba la vista index de Vlan Group"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.vlan_1 = VlanGroup.objects.create(
            name='Guayas',
            description='04',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.vlan_2 = VlanGroup.objects.create(
            name='Pichincha',
            description='07',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.vlan_3 = VlanGroup.objects.create(
            name='Playas',
            description='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_vlan_groups(self):
        """Prueba que las Vlan Group se puedan ver paginadas en el template"""
        response = self.client.get('/network/vlan-group')
        self.assertContains(response, self.vlan_3.name)
        self.assertContains(response, self.vlan_2.name)
        self.assertContains(response, self.vlan_1.name)

    def test_that_it_can_be_filtered_by_vlan_group_name(self):
        """Prueba que las Vlan Group se puedan filtrar por nombre"""
        response = self.client.get('/network/vlan-group?search=guay')
        self.assertNotContains(response, self.vlan_3.name)
        self.assertNotContains(response, self.vlan_2.name)
        self.assertContains(response, self.vlan_1.name)
