""" Vlan Group Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from networks.models import VlanGroup


class UpdateVlanGroupTest(TestCase):
    """Prueba que se actualize una provincia"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        model = VlanGroup.objects.create(name='Guyas', description='27')

        response = self.client.post('/network/vlan-group/edit/{}'.format(model.pk), {
            'name': 'G',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(VlanGroup.objects.filter(name='G').exists())
        self.assertTrue(VlanGroup.objects.filter(name='Guyas').exists())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        model = VlanGroup.objects.create(name='Guyas', description='27')

        response = self.client.post('/network/vlan-group/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(VlanGroup.objects.filter(description='0').exists())

    def test_update_only_name_vlan_group(self):
        """Prueba la atualización de una provincia"""
        model = VlanGroup.objects.create(name='Guyas', description='04')

        self.client.post('/network/vlan-group/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'description': '04'
        })

        self.assertTrue(VlanGroup.objects.filter(name='Guayas', description='04').exists())
        self.assertFalse(VlanGroup.objects.filter(name='Guyas', description='04').exists())

    def test_update_only_description_vlan_group(self):
        """Prueba la atualización de una provincia"""
        model = VlanGroup.objects.create(name='Guayas', description='12')

        self.client.post('/network/vlan-group/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'description': '04'
        })

        self.assertTrue(VlanGroup.objects.filter(name='Guayas', description='04').exists())
        self.assertFalse(VlanGroup.objects.filter(name='Guayas', description='12').exists())

    def test_update_a_vlan_group(self):
        """Prueba la atualización de una provincia"""
        model = VlanGroup.objects.create(name='Guyas', description='27')

        self.client.post('/network/vlan-group/edit/{}'.format(model.pk), {
            'name': 'Guayas',
            'description': '04'
        })

        self.assertTrue(VlanGroup.objects.filter(name='Guayas', description='04').exists())
        self.assertFalse(VlanGroup.objects.filter(name='Guyas', description='27').exists())
