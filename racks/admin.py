from django.contrib import admin
from racks.models import *

# Register your models here.
admin.site.register(RackType)
admin.site.register(Brand)
admin.site.register(Model)
admin.site.register(Rack)
admin.site.register(Inverter)
admin.site.register(Battery)
