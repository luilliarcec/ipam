from django.apps import AppConfig


class RackConfig(AppConfig):
    name = 'racks'
