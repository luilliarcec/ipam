""" Batteries Forms """

# Django
from django import forms
# App
from racks.models import Battery, Brand
from locations.models import Node


class BatteryForm(forms.ModelForm):
    """Formulario y validacion de beterias"""
    name = forms.CharField(min_length=2)
    installed_at = forms.DateField()
    duration_time = forms.IntegerField()
    node = forms.ModelChoiceField(required=True, queryset=Node.objects.all())
    brand = forms.ModelChoiceField(required=True, queryset=Brand.objects.all())

    def clean(self):
        cleaned_data = super(BatteryForm, self).clean()
        return cleaned_data

    class Meta:
        model = Battery
        fields = ['node', 'brand', 'name', 'installed_at', 'duration_time']
