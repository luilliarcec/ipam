""" Batteries Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Battery, Brand
from locations.models import *


class DeleteBatteryTest(TestCase):
    """Prueba que se elimine un Batería"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_battery(self):
        """Prueba la eliminación de un Batería"""
        guayas = Province.objects.create(name='Playas', code='04')
        milagro = Canton.objects.create(name='Salinas', province=guayas)

        node = Node.objects.create(
            canton=milagro, name='node_san_salinas', level='3', address='por ahí',
            coordinates='0000', contact_name='Sper Man', contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )
        brand = Brand.objects.create(maker='RockStar', description='Nanana')

        model = Battery.objects.create(name='Batería Faker', brand=brand, node=node,
                                       installed_at=datetime.now().date())

        self.client.delete('/rack/battery/delete/{}'.format(model.pk))

        self.assertFalse(Battery.objects.filter(name='Batería Faker').exists())
