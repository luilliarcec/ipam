""" Batteries Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Battery, Brand
from locations.models import *


class ListBatteryTest(TestCase):
    """Prueba la vista index de batteries"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Guayas', code='04')
        milagro = Canton.objects.create(name='Milagro', province=guayas)

        node = Node.objects.create(
            canton=milagro,
            name='node_san_iguana',
            level='3',
            address='por ahí',
            coordinates='0000',
            contact_name='Sper Man',
            contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )

        brand = Brand.objects.create(maker='RockStar', description='Nanana')

        self.batteries_250w = Battery.objects.create(
            name='Battery de 250w',
            node=node,
            brand=brand,
            duration_time=6,
            installed_at=datetime.now() - timedelta(days=2),
            created_at=datetime.now() - timedelta(days=2)
        )

        self.batteries_120 = Battery.objects.create(
            name='batteries_120',
            node=node,
            brand=brand,
            duration_time=6,
            installed_at=datetime.now() - timedelta(days=1),
            created_at=datetime.now() - timedelta(days=1)
        )

        self.batteries_24w = Battery.objects.create(
            name='batteries_24w',
            node=node,
            brand=brand,
            duration_time=6,
            installed_at=datetime.now() + timedelta(days=1),
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_batteries(self):
        """Prueba que las Batterys se puedan ver paginadas en el template"""
        response = self.client.get('/rack/battery')
        self.assertContains(response, self.batteries_24w.name)
        self.assertContains(response, self.batteries_120.name)
        self.assertContains(response, self.batteries_250w.name)

    def test_that_it_can_be_filtered_by_Battery_name(self):
        """Prueba que las Batterys se puedan filtrar por nombre"""
        response = self.client.get('/rack/battery?search=250w')
        self.assertNotContains(response, self.batteries_24w.name)
        self.assertNotContains(response, self.batteries_120.name)
        self.assertContains(response, self.batteries_250w.name)
