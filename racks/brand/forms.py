""" Brands Forms """

# Django
from django import forms
# App
from racks.models import Brand


class BrandForm(forms.ModelForm):
    """Formulario y validacion de marcas"""
    maker = forms.CharField(min_length=2)
    description = forms.CharField(min_length=2)

    def clean(self):
        cleaned_data = super(BrandForm, self).clean()
        return cleaned_data

    class Meta:
        model = Brand
        fields = ['maker', 'description']
