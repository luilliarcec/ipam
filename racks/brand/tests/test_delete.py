""" Brands Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Brand


class DeleteBrandTest(TestCase):
    """Prueba que se elimine una marca"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_brand(self):
        """Prueba la eliminación de una provincia"""
        model = Brand.objects.create(maker='Mikrotik', description='No sé')

        self.client.delete('/rack/brand/delete/{}'.format(model.pk))

        self.assertFalse(Brand.objects.filter(maker='Mikrotik').exists())
