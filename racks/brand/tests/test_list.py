""" Brands Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Brand


class ListBrandTest(TestCase):
    """Prueba la vista index de Marcas"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.huaweii = Brand.objects.create(
            maker='Huaweii',
            description='04',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.mikrotik = Brand.objects.create(
            maker='Mikrotik',
            description='07',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.ubiquiti = Brand.objects.create(
            maker='Ubiquiti',
            description='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_brands(self):
        """Prueba que las marcas se puedan ver paginadas en el template"""
        response = self.client.get('/rack/brand')
        self.assertContains(response, self.ubiquiti)
        self.assertContains(response, self.mikrotik)
        self.assertContains(response, self.huaweii)

    def test_that_it_can_be_filtered_by_brand_maker(self):
        """Prueba que las marcas se puedan filtrar por nombre"""
        response = self.client.get('/rack/brand?search=huaw')
        self.assertNotContains(response, self.ubiquiti)
        self.assertNotContains(response, self.mikrotik)
        self.assertContains(response, self.huaweii)
