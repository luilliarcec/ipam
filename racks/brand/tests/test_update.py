""" Brands Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Brand


class UpdateBrandTest(TestCase):
    """Prueba que se actualiza la marca"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.model = Brand.objects.create(maker='Mikrotik', description='No sé')

    def test_validate_that_the_maker_is_not_less_than_2_chars(self):
        """Valida que haya un error en maker"""
        response = self.client.post('/rack/brand/edit/{}'.format(self.model.pk), {
            'maker': 'U',
            'description': 'Bla Bla Bla'
        })

        self.assertFormError(
            response,
            'form',
            'maker',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Brand.objects.filter(maker='Ubiquiti').exists())

    def test_validate_that_the_maker_is_unique(self):
        """Valida que haya un error en maker"""
        Brand.objects.create(maker='Ubiquiti', description='07')

        response = self.client.post('/rack/brand/edit/{}'.format(self.model.pk), {
            'maker': 'Ubiquiti',
            'description': '04'
        })

        self.assertFormError(
            response,
            'form',
            'maker',
            'Ya existe Marca con este Maker.'
        )
        self.assertEqual(1, Brand.objects.filter(maker='Ubiquiti').count())

    def test_validate_that_the_description_is_not_less_than_2_chars(self):
        """Valida que haya un error en description"""
        response = self.client.post('/rack/brand/edit/{}'.format(self.model.pk), {
            'maker': 'Ubiquiti',
            'description': '0'
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )
        self.assertFalse(Brand.objects.filter(description='0').exists())

    def test_validate_that_the_maker_and_description_are_require(self):
        """Valida que haya un error en description"""
        response = self.client.post('/rack/brand/edit/{}'.format(self.model.pk), {
            'maker': '',
            'description': ''
        })

        self.assertFormError(
            response,
            'form',
            'description',
            'Este campo es obligatorio.'
        )

        self.assertFormError(
            response,
            'form',
            'maker',
            'Este campo es obligatorio.'
        )

        self.assertEqual(1, Brand.objects.count())

    def test_create_a_update_brand(self):
        """Prueba la creación de una nueva provincia"""
        response = self.client.post('/rack/brand/edit/{}'.format(self.model.pk), {
            'maker': 'Ubiquiti',
            'description': 'Marca de dispositivos'
        })
        self.assertTrue(Brand.objects.filter(maker='Ubiquiti').exists())
