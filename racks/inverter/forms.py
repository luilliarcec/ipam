""" Inverters Forms """

# Django
from django import forms
# App
from racks.models import Inverter, Brand
from locations.models import Node


class InverterForm(forms.ModelForm):
    """Formulario y validacion de inversores"""
    name = forms.CharField(min_length=2)
    installed_at = forms.DateField()
    node = forms.ModelChoiceField(required=True, queryset=Node.objects.all())
    brand = forms.ModelChoiceField(required=True, queryset=Brand.objects.all())

    def clean(self):
        cleaned_data = super(InverterForm, self).clean()
        return cleaned_data

    class Meta:
        model = Inverter
        fields = ['node', 'brand', 'name', 'installed_at']
