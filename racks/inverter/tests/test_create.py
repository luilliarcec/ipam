""" Inverters Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Inverter, Brand
from locations.models import *


class CreateInverterTest(TestCase):
    """Prueba que se cree el inverter"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Guayas', code='04')
        milagro = Canton.objects.create(name='Milagro', province=guayas)

        self.node = Node.objects.create(
            canton=milagro, name='node_san_iguana', level='3', address='por ahí',
            coordinates='0000', contact_name='Sper Man', contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )
        self.brand = Brand.objects.create(maker='RockStar', description='Nanana')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/rack/inverter/create', {
            'name': 'U',
            'node': self.node.pk,
            'brand': self.brand.pk,
            'installed_at': datetime.now().date(),
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Inverter.objects.filter(name='U').exists())

    def test_validate_that_the_name_and_node_and_brand_are_require(self):
        """Valida que haya un error en description"""
        response = self.client.post('/rack/inverter/create', {
            'name': '',
            'node': '',
            'brand': '',
            'installed_at': datetime.now().date(),
        })

        self.assertFormError(response, 'form', 'node', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'name', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'brand', 'Este campo es obligatorio.')

        self.assertEqual(0, Inverter.objects.count())

    def test_validate_that_the_date_installed_is_a_date_valid(self):
        """Valida que haya un error en installed_at"""
        response = self.client.post('/rack/inverter/create', {
            'name': 'Inversor 24W',
            'node': self.node.pk,
            'brand': self.brand.pk,
            'installed_at': '25/ab/2018',
        })

        self.assertFormError(response, 'form', 'installed_at', 'Introduzca una fecha válida.')

        self.assertEqual(0, Inverter.objects.count())

    def test_validate_that_the_node_and_brand_are_exists_in_database(self):
        """Valida que haya un error en nodo y marca"""
        response = self.client.post('/rack/inverter/create', {
            'name': 'Moito Benno',
            'node': 'A',
            'brand': 'B',
            'installed_at': datetime.now().date(),
        })

        self.assertFormError(response, 'form', 'node',
                             'Escoja una opción válida. Esa opción no está entre las disponibles.')

        self.assertFormError(response, 'form', 'brand',
                             'Escoja una opción válida. Esa opción no está entre las disponibles.')

        self.assertEqual(0, Inverter.objects.count())

    def test_create_a_new_brand(self):
        """Prueba la creación de una nueva provincia"""
        response = self.client.post('/rack/inverter/create', {
            'name': 'Inversor 24W',
            'node': self.node.pk,
            'brand': self.brand.pk,
            'installed_at': datetime.now().date(),
        })
        self.assertTrue(Inverter.objects.filter(name='Inversor 24W').exists())
