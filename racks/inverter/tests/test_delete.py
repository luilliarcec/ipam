""" Inverters Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Inverter, Brand
from locations.models import *


class DeleteInverterTest(TestCase):
    """Prueba que se elimine un inversor"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_inverter(self):
        """Prueba la eliminación de un inversor"""
        guayas = Province.objects.create(name='Playas', code='04')
        milagro = Canton.objects.create(name='Salinas', province=guayas)

        node = Node.objects.create(
            canton=milagro, name='node_san_salinas', level='3', address='por ahí',
            coordinates='0000', contact_name='Sper Man', contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )
        brand = Brand.objects.create(maker='RockStar', description='Nanana')

        model = Inverter.objects.create(name='Inversor Faker', brand=brand, node=node,
                                             installed_at=datetime.now().date())

        self.client.delete('/rack/inverter/delete/{}'.format(model.pk))

        self.assertFalse(Inverter.objects.filter(name='Inversor Faker').exists())
