""" Inverters Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Inverter, Brand
from locations.models import *


class ListInverterTest(TestCase):
    """Prueba la vista index de inverters"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Guayas', code='04')
        milagro = Canton.objects.create(name='Milagro', province=guayas)

        node = Node.objects.create(
            canton=milagro,
            name='node_san_iguana',
            level='3',
            address='por ahí',
            coordinates='0000',
            contact_name='Sper Man',
            contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )

        brand = Brand.objects.create(maker='RockStar', description='Nanana')

        self.inverter_250w = Inverter.objects.create(
            name='Inverter de 250w',
            node=node,
            brand=brand,
            installed_at=datetime.now() - timedelta(days=2),
            created_at=datetime.now() - timedelta(days=2)
        )

        self.inverter_120 = Inverter.objects.create(
            name='inverter_120',
            node=node,
            brand=brand,
            installed_at=datetime.now() - timedelta(days=1),
            created_at=datetime.now() - timedelta(days=1)
        )

        self.inverter_24w = Inverter.objects.create(
            name='inverter_24w',
            node=node,
            brand=brand,
            installed_at=datetime.now() + timedelta(days=1),
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_inverters(self):
        """Prueba que las inverters se puedan ver paginadas en el template"""
        response = self.client.get('/rack/inverter')
        self.assertContains(response, self.inverter_24w)
        self.assertContains(response, self.inverter_120)
        self.assertContains(response, self.inverter_250w)

    def test_that_it_can_be_filtered_by_inverter_name(self):
        """Prueba que las inverters se puedan filtrar por nombre"""
        response = self.client.get('/rack/inverter?search=250w')
        self.assertNotContains(response, self.inverter_24w)
        self.assertNotContains(response, self.inverter_120)
        self.assertContains(response, self.inverter_250w)
