""" Inverters Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from racks.inverter.forms import InverterForm
from racks.models import Inverter, Brand
from locations.models import Node


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista los inversores"""
    template_name = 'racks/inverters/index.html'
    model = Inverter
    paginate_by = 15
    context_object_name = 'inverters'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Inverter.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea un inversor"""
    model = Inverter
    template_name = 'racks/inverters/create.html'
    form_class = InverterForm
    success_url = reverse_lazy('racks:inverter.index')
    attributes = {'name': '', 'installed_at': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_brand'] = self.post_old_data('brand')
        context['old_node'] = self.post_old_data('node')
        context['brands'] = Brand.objects.all()
        context['nodes'] = Node.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza un inversor"""
    model = Inverter
    template_name = 'racks/inverters/edit.html'
    form_class = InverterForm
    success_url = reverse_lazy('racks:inverter.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'installed_at': self.get_object().installed_at.isoformat
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_brand'] = self.post_old_data('brand', self.get_object().brand.pk)
        context['old_node'] = self.post_old_data('node', self.get_object().node.pk)
        context['brands'] = Brand.objects.all()
        context['nodes'] = Node.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina un inversor"""
    model = Inverter
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
