""" Models Forms """

# Django
from django import forms
# Models
from racks.models import Brand, Model


class ModelForm(forms.ModelForm):
    """Formulario y validacion de modelos"""
    brand = forms.ModelChoiceField(required=True, queryset=Brand.objects.all())
    name = forms.CharField(min_length=2, max_length=120)

    def clean(self):
        cleaned_data = super(ModelForm, self).clean()
        return cleaned_data

    class Meta:
        model = Model
        fields = ['name', 'description', 'brand']
