""" Models Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Model, Brand


class CreateModelTest(TestCase):
    """Prueba que se cree el cantón"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.brand = Brand.objects.create(maker='Ubiquiti', description='NaNaNa')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/rack/model/create', {
            'name': 'G',
            'brand': self.brand.pk,
            'description': 'Nan'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Model.objects.filter(name='G').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        Model.objects.create(name='Guayaquil', brand=self.brand)

        response = self.client.post('/rack/model/create', {
            'name': 'Guayaquil',
            'brand': self.brand.pk,
            'description': 'Nan'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Modelo con este Name.'
        )

        self.assertEqual(1, Model.objects.filter(name='Guayaquil').count())

    def test_validate_that_the_brand_exists_in_database(self):
        """Valida que haya un error en Model"""
        response = self.client.post('/rack/model/create', {
            'name': 'Guayaquil',
            'brand': 'abc',
            'description': 'Nan'
        })

        self.assertFormError(
            response,
            'form',
            'brand',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

        self.assertFalse(Model.objects.filter(name='Guayaquil').exists())

    def test_validate_that_the_name_and_brand_are_required(self):
        """Valida que haya un error en Model"""
        response = self.client.post('/rack/model/create', {
            'name': '',
            'brand': '',
            'description': 'Nan'
        })

        self.assertFormError(response, 'form', 'name', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'brand', 'Este campo es obligatorio.')

        self.assertEqual(0, Model.objects.count())

    def test_create_a_new_Model(self):
        """Prueba la creación de una nueva cantón"""
        response = self.client.post('/rack/model/create', {
            'name': 'Guayaquil',
            'brand': self.brand.pk,
            'description': 'Nan'
        })

        self.assertTrue(Model.objects.filter(name='Guayaquil').exists())
