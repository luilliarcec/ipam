""" Models Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Model, Brand


class DeleteModelTest(TestCase):
    """Prueba que se elimine una marca"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_model(self):
        """Prueba la eliminación de una provincia"""
        fake_brand = Brand.objects.create(maker='Fake', description='Marca Fake')
        model = Model.objects.create(name='Mikrotik', description='No sé', brand=fake_brand)

        self.client.delete('/rack/model/delete/{}'.format(model.pk))

        self.assertFalse(Model.objects.filter(name='Mikrotik').exists())
