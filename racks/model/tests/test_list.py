""" Models Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Model, Brand


class ListModelTest(TestCase):
    """Prueba la vista index de Marcas"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        ubi = Brand.objects.create(maker='Ubiquiti', description='nanana')
        mik = Brand.objects.create(maker='Mikrotik', description='nanana')

        self.p19 = Model.objects.create(
            name='p19',
            brand=mik,
            description='04',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.ak47 = Model.objects.create(
            name='ak47',
            brand=ubi,
            description='07',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.minigun = Model.objects.create(
            name='minigun',
            brand=ubi,
            description='05',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_models(self):
        """Prueba que las marcas se puedan ver paginadas en el template"""
        response = self.client.get('/rack/model')
        self.assertContains(response, self.minigun)
        self.assertContains(response, self.ak47)
        self.assertContains(response, self.p19)

    def test_that_it_can_be_filtered_by_brand_maker(self):
        """Prueba que las marcas se puedan filtrar por nombre"""
        response = self.client.get('/rack/model?search=mik')
        self.assertNotContains(response, self.minigun)
        self.assertNotContains(response, self.ak47)
        self.assertContains(response, self.p19)
