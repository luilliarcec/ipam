""" Models Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Model, Brand


class UpdateModelTest(TestCase):
    """Prueba que se actualize el cantón"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.brand = Brand.objects.create(maker='Ubiquiti', description='Marca Ubiquiti')
        fake_brand = Brand.objects.create(maker='Fake', description='Marca Fake')
        self.model = Model.objects.create(name='AK47', description='Na na na', brand=fake_brand)

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/rack/model/edit/{}'.format(self.model.pk), {
            'name': 'G',
            'brand': self.brand.pk,
            'description': 'Nan'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Model.objects.filter(name='G').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        Model.objects.create(name='Guayaquil', brand=self.brand, description='Perpe')

        response = self.client.post('/rack/model/edit/{}'.format(self.model.pk), {
            'name': 'Guayaquil',
            'brand': self.brand.pk,
            'description': 'Nan'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Modelo con este Name.'
        )

        self.assertEqual(1, Model.objects.filter(name='Guayaquil').count())

    def test_validate_that_the_brand_exists_in_database(self):
        """Valida que haya un error en Model"""
        response = self.client.post('/rack/model/edit/{}'.format(self.model.pk), {
            'name': 'Guayaquil',
            'brand': 'abc',
            'description': 'Nan'
        })

        self.assertFormError(
            response,
            'form',
            'brand',
            'Escoja una opción válida. Esa opción no está entre las disponibles.'
        )

        self.assertFalse(Model.objects.filter(name='Guayaquil').exists())

    def test_validate_that_the_name_and_brand_are_required(self):
        """Valida que haya un error en Model"""
        response = self.client.post('/rack/model/edit/{}'.format(self.model.pk), {
            'name': '',
            'brand': '',
            'description': 'Nan'
        })

        self.assertFormError(response, 'form', 'name', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'brand', 'Este campo es obligatorio.')

        self.assertEqual(1, Model.objects.count())

    def test_update_a_model(self):
        """Prueba la creación de una nueva cantón"""
        response = self.client.post('/rack/model/edit/{}'.format(self.model.pk), {
            'name': 'Guayaquil',
            'brand': self.brand.pk,
            'description': 'Nan'
        })

        self.assertTrue(Model.objects.filter(name='Guayaquil').exists())
