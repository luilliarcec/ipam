""" Models Views """

# Django
from django.db.models import Q
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from racks.model.forms import ModelForm
from racks.models import Model, Brand


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista los modelos"""
    template_name = 'racks/models/index.html'
    model = Model
    paginate_by = 15
    context_object_name = 'models'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Model.objects.filter(
            Q(name__icontains=search) |
            Q(brand__maker__icontains=search)
        ).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea un modelo"""
    model = Model
    template_name = 'racks/models/create.html'
    form_class = ModelForm
    success_url = reverse_lazy('racks:model.index')
    attributes = {'name': '', 'description': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_brand'] = self.post_old_data(name='brand')
        context['brands'] = Brand.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza un modelo"""
    model = Model
    template_name = 'racks/models/edit.html'
    form_class = ModelForm
    success_url = reverse_lazy('racks:model.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'description': self.get_object().description
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_brand'] = self.post_old_data(name='brand', value=self.get_object().brand.pk)
        context['brands'] = Brand.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina un modelo"""
    model = Model
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
