from django.db import models
from locations.models import Node


class RackType(models.Model):
    name = models.CharField(max_length=50, unique=True)
    urs = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Tipo de rack'
        verbose_name_plural = 'Tipos de rack'
        ordering = ('name',)


class Brand(models.Model):
    maker = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.maker)

    class Meta:
        verbose_name = 'Marca'
        verbose_name_plural = 'Marcas'
        ordering = ('maker',)


class Model(models.Model):
    brand = models.ForeignKey(Brand, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Modelo'
        verbose_name_plural = 'Modelos'
        ordering = ('name',)


class Rack(models.Model):
    node = models.ForeignKey(Node, null=True, blank=True, on_delete=models.CASCADE)
    rack_type = models.ForeignKey(RackType, null=True, blank=True, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    hight = models.CharField(max_length=50)
    width = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Rack'
        verbose_name_plural = 'Racks'
        ordering = ('name',)


class Inverter(models.Model):
    node = models.ForeignKey(Node, null=True, blank=True, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    installed_at = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Inversor'
        verbose_name_plural = 'Inversores'
        ordering = ('name',)


class Battery(models.Model):
    node = models.ForeignKey(Node, null=True, blank=True, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    installed_at = models.DateField(null=True)
    duration_time = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Batería'
        verbose_name_plural = 'Baterías'
        ordering = ('name',)
