""" Rack Forms """

# Django
from django import forms
# App
from racks.models import Rack, Brand, RackType
from locations.models import Node


class RackForm(forms.ModelForm):
    """Formulario y validacion de racks"""
    name = forms.CharField(min_length=2)
    hight = forms.DecimalField()
    width = forms.DecimalField()
    node = forms.ModelChoiceField(required=True, queryset=Node.objects.all())
    brand = forms.ModelChoiceField(required=True, queryset=Brand.objects.all())
    rack_type = forms.ModelChoiceField(required=True, queryset=RackType.objects.all())

    def clean(self):
        cleaned_data = super(RackForm, self).clean()
        return cleaned_data

    class Meta:
        model = Rack
        fields = ['node', 'brand', 'rack_type', 'name', 'hight', 'width']
