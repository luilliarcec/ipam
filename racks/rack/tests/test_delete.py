""" Rack Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Rack, Brand, RackType
from locations.models import *


class DeleteRackTest(TestCase):
    """Prueba que se elimine un rack"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_Rack(self):
        """Prueba la eliminación de un rack"""
        guayas = Province.objects.create(name='Playas', code='04')
        milagro = Canton.objects.create(name='Salinas', province=guayas)

        node = Node.objects.create(
            canton=milagro, name='node_san_salinas', level='3', address='por ahí',
            coordinates='0000', contact_name='Sper Man', contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )
        brand = Brand.objects.create(maker='RockStar', description='Nanana')

        rack_type = RackType.objects.create(name='Rack 24UR', urs=24, color='negro')

        model = Rack.objects.create(
            name='Rack Faker', node=node, brand=brand,
            rack_type=rack_type, hight=1.1, width=1.1
        )

        self.client.delete('/rack/delete/{}'.format(model.pk))

        self.assertFalse(Rack.objects.filter(name='Rack Faker').exists())
