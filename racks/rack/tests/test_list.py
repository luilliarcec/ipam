""" Rack Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Rack, Brand, RackType
from locations.models import *


class ListRackTest(TestCase):
    """Prueba la vista index de batteries"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Guayas', code='04')
        milagro = Canton.objects.create(name='Milagro', province=guayas)

        node = Node.objects.create(
            canton=milagro,
            name='node_san_iguana',
            level='3',
            address='por ahí',
            coordinates='0000',
            contact_name='Sper Man',
            contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )

        brand = Brand.objects.create(maker='RockStar', description='Nanana')
        rack_type = RackType.objects.create(name='Rack 24UR', urs=24, color='negro')

        self.rack_1 = Rack.objects.create(
            name='Rack de Cama',
            node=node,
            brand=brand,
            rack_type=rack_type,
            hight=12.5,
            width=7.8,
            created_at=datetime.now() - timedelta(days=2)
        )

        self.rack_2 = Rack.objects.create(
            name='rack_2',
            node=node,
            brand=brand,
            rack_type=rack_type,
            hight=12.5,
            width=7.8,
            created_at=datetime.now() - timedelta(days=1)
        )

        self.rack_3 = Rack.objects.create(
            name='rack_3',
            node=node,
            brand=brand,
            rack_type=rack_type,
            hight=12.5,
            width=7.8,
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_racks(self):
        """Prueba que las racks se puedan ver paginadas en el template"""
        response = self.client.get('/rack/')
        self.assertContains(response, self.rack_3)
        self.assertContains(response, self.rack_2)
        self.assertContains(response, self.rack_1)

    def test_that_it_can_be_filtered_by_Rack_name(self):
        """Prueba que las racks se puedan filtrar por nombre"""
        response = self.client.get('/rack/?search=cam')
        self.assertNotContains(response, self.rack_3)
        self.assertNotContains(response, self.rack_2)
        self.assertContains(response, self.rack_1)
