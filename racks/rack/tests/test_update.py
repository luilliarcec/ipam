""" Rack Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import Rack, Brand, RackType
from locations.models import *


class UpdateRackTest(TestCase):
    """Prueba que se actualiza un Rack"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        guayas = Province.objects.create(name='Playas', code='04')
        milagro = Canton.objects.create(name='Salinas', province=guayas)

        self.node = Node.objects.create(
            canton=milagro, name='node_san_salinas', level='3', address='por ahí',
            coordinates='0000', contact_name='Sper Man', contact_phone='0000',
            installed_at=datetime.now() - timedelta(days=38),
            created_at=datetime.now() - timedelta(days=38)
        )
        self.brand = Brand.objects.create(maker='RockStar', description='Nanana')
        self.rack_type = RackType.objects.create(name='Rack 24UR', urs=24, color='negro')

        self.model = Rack.objects.create(
            name='Rack Faker', node=self.node, brand=self.brand,
            rack_type=self.rack_type, hight=12.5, width=7.8,
        )

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en name"""
        response = self.client.post('/rack/edit/{}'.format(self.model.pk), {
            'name': 'U',
            'node': self.node.pk,
            'brand': self.brand.pk,
            'rack_type': self.rack_type.pk,
            'hight': 25.5,
            'width': 20,
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(Rack.objects.filter(name='U').exists())

    def test_validate_that_the_name_and_node_and_brand_are_require(self):
        """Valida que haya un error en name, nodo y brand"""
        response = self.client.post('/rack/edit/{}'.format(self.model.pk), {
            'name': '',
            'node': '',
            'brand': '',
            'rack_type': '',
            'hight': '',
            'width': '',
        })

        self.assertFormError(response, 'form', 'node', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'name', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'brand', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'rack_type', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'hight', 'Este campo es obligatorio.')
        self.assertFormError(response, 'form', 'width', 'Este campo es obligatorio.')

        self.assertTrue(Rack.objects.filter(name='Rack Faker').exists())

    def test_validate_that_the_hight_and_width_are_decimal(self):
        """Valida que haya un error en installed_at"""
        response = self.client.post('/rack/edit/{}'.format(self.model.pk), {
            'name': 'Rack 24W',
            'node': self.node.pk,
            'brand': self.brand.pk,
            'rack_type': self.rack_type.pk,
            'hight': '25A',
            'width': '27B',
        })

        self.assertFormError(response, 'form', 'hight', 'Introduzca un número.')
        self.assertFormError(response, 'form', 'width', 'Introduzca un número.')

        self.assertTrue(Rack.objects.filter(name='Rack Faker'))

    def test_validate_that_the_node_and_brand_are_exists_in_database(self):
        """Valida que haya un error en nodo y marca"""
        response = self.client.post('/rack/edit/{}'.format(self.model.pk), {
            'name': 'Moito Benno',
            'node': 'A',
            'brand': 'B',
            'rack_type': '8',
            'hight': 25.5,
            'width': 20,
        })

        self.assertFormError(response, 'form', 'node',
                             'Escoja una opción válida. Esa opción no está entre las disponibles.')

        self.assertFormError(response, 'form', 'brand',
                             'Escoja una opción válida. Esa opción no está entre las disponibles.')

        self.assertFormError(response, 'form', 'rack_type',
                             'Escoja una opción válida. Esa opción no está entre las disponibles.')

        self.assertTrue(Rack.objects.filter(name='Rack Faker'))

    def test_update_a_Rack(self):
        """Prueba la creación de una nueva provincia"""
        response = self.client.post('/rack/edit/{}'.format(self.model.pk), {
            'name': 'Rack 24 UR',
            'node': self.node.pk,
            'brand': self.brand.pk,
            'rack_type': self.rack_type.pk,
            'hight': 25.5,
            'width': 20,
        })
        self.assertTrue(Rack.objects.filter(name='Rack 24 UR').exists())
