""" Rack Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from racks.rack.forms import RackForm
from racks.models import Rack, Brand, RackType
from locations.models import Node


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista los racks"""
    template_name = 'racks/index.html'
    model = Rack
    paginate_by = 15
    context_object_name = 'racks'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return Rack.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea un rack"""
    model = Rack
    template_name = 'racks/create.html'
    form_class = RackForm
    success_url = reverse_lazy('racks:rack.index')
    attributes = {'name': '', 'hight': '', 'width': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['old_brand'] = self.post_old_data('brand')
        context['old_node'] = self.post_old_data('node')
        context['old_rack_type'] = self.post_old_data('rack_type')
        context['brands'] = Brand.objects.all()
        context['nodes'] = Node.objects.all()
        context['rack_types'] = RackType.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza un rack"""
    model = Rack
    template_name = 'racks/edit.html'
    form_class = RackForm
    success_url = reverse_lazy('racks:rack.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'hight': self.get_object().hight,
            'width': self.get_object().width
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['old_brand'] = self.post_old_data('brand', self.get_object().brand.pk)
        context['old_node'] = self.post_old_data('node', self.get_object().node.pk)
        context['old_rack_type'] = self.post_old_data('rack_type', self.get_object().rack_type.pk)
        context['brands'] = Brand.objects.all()
        context['nodes'] = Node.objects.all()
        context['rack_types'] = RackType.objects.all()
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina un rack"""
    model = Rack
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
