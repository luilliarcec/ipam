""" RackTypes Forms """

# Django
from django import forms
# App
from racks.models import RackType


class RackTypeForm(forms.ModelForm):
    """Formulario y validacion de tipos de rack"""
    name = forms.CharField(min_length=2, max_length=120)
    urs = forms.IntegerField()

    def clean(self):
        cleaned_data = super(RackTypeForm, self).clean()
        return cleaned_data

    class Meta:
        model = RackType
        fields = ['name', 'urs', 'color']
