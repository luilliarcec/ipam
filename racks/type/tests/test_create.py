""" RackTypes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import RackType


class CreateRackTypeTest(TestCase):
    """Prueba que se cree el tipo de rack"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_validate_that_the_name_is_not_less_than_2_chars(self):
        """Valida que haya un error en maker"""
        response = self.client.post('/rack/type/create', {
            'name': 'U',
            'urs': 2,
            'color': 'negro'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Asegúrese de que este valor tenga al menos 2 caracteres (tiene 1).'
        )

        self.assertFalse(RackType.objects.filter(name='U').exists())

    def test_validate_that_the_name_is_unique(self):
        """Valida que haya un error en name"""
        RackType.objects.create(name='Rack 24UR', urs=24, color='negro')

        response = self.client.post('/rack/type/create', {
            'name': 'Rack 24UR',
            'urs': 2,
            'color': 'negro'
        })

        self.assertFormError(
            response,
            'form',
            'name',
            'Ya existe Tipo de rack con este Name.'
        )
        self.assertEqual(1, RackType.objects.filter(name='Rack 24UR').count())

    def test_validate_that_the_urs_is_integer(self):
        """Valida que haya un error en urs"""
        response = self.client.post('/rack/type/create', {
            'name': 'Rack 24UR',
            'urs': '0AB',
            'color': 'negro'
        })

        self.assertFormError(
            response,
            'form',
            'urs',
            'Introduzca un número entero.'
        )
        self.assertFalse(RackType.objects.filter(name='Rack 24UR').exists())

    def test_validate_that_the_name_and_urs_are_require(self):
        """Valida que haya un error en description"""
        response = self.client.post('/rack/type/create', {
            'name': '',
            'urs': ''
        })

        self.assertFormError(
            response,
            'form',
            'urs',
            'Este campo es obligatorio.'
        )

        self.assertFormError(
            response,
            'form',
            'name',
            'Este campo es obligatorio.'
        )

        self.assertEqual(0, RackType.objects.count())

    def test_create_a_new_type(self):
        """Prueba la creación de una nueva provincia"""
        response = self.client.post('/rack/type/create', {
            'name': 'RACK 24 UR',
            'urs': 24,
            'color': 'negro'
        })
        self.assertTrue(RackType.objects.filter(name='RACK 24 UR').exists())
