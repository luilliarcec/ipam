""" RackTypes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import RackType


class DeleteRackTypeTest(TestCase):
    """Prueba que se elimine un tipo"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

    def test_delete_a_racktype(self):
        """Prueba la eliminación de una provincia"""
        model = RackType.objects.create(name='Rack 24UR', urs=24, color='negro')

        self.client.delete('/rack/type/delete/{}'.format(model.pk))

        self.assertFalse(RackType.objects.filter(name='Rack 24UR').exists())
