""" RackTypes Tests """

# Django
from django.contrib.auth.models import User
from django.test import TestCase
# App
from datetime import *
from racks.models import RackType


class ListRackTypeTest(TestCase):
    """Lista de tipos de rack"""

    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test1234**')
        user.save()
        self.client.login(username='test', password='test1234**')

        self.rack1 = RackType.objects.create(
            name='Rackata',
            urs=4,
            color='negro',
            created_at=datetime.now() - timedelta(days=2)
        )

        self.rack2 = RackType.objects.create(
            name='Rack 2',
            urs=4,
            color='negro',
            created_at=datetime.now() - timedelta(days=1)
        )

        self.rack3 = RackType.objects.create(
            name='Rack 3',
            urs=4,
            color='negro',
            created_at=datetime.now() + timedelta(days=1)
        )

    def test_that_you_can_see_the_list_of_racktypes(self):
        """Prueba que las provincias se puedan ver paginadas en el template"""
        response = self.client.get('/rack/type')
        self.assertContains(response, self.rack3)
        self.assertContains(response, self.rack2)
        self.assertContains(response, self.rack1)

    def test_that_it_can_be_filtered_by_racktype_name(self):
        """Prueba que las provincias se puedan filtrar por nombre"""
        response = self.client.get('/rack/type?search=racka')
        self.assertNotContains(response, self.rack3)
        self.assertNotContains(response, self.rack2)
        self.assertContains(response, self.rack1)
