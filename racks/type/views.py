""" RackTypes Views """

# Django
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# App
from utils.mixins import OldDataMixin
from racks.type.forms import RackTypeForm
from racks.models import RackType


class Index(LoginRequiredMixin, ListView, OldDataMixin):
    """Lista los tipos de rack"""
    template_name = 'racks/types/index.html'
    model = RackType
    paginate_by = 15
    context_object_name = 'types'
    attributes = {'search': ''}

    def get_queryset(self):
        search = self.get_old_data('search')
        return RackType.objects.filter(name__icontains=search).order_by('-created_at')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return self.get_all_olds_datas(context=context, attributes=self.attributes)


class Create(LoginRequiredMixin, CreateView, OldDataMixin):
    """Crea un tipo de rack"""
    model = RackType
    template_name = 'racks/types/create.html'
    form_class = RackTypeForm
    success_url = reverse_lazy('racks:type.index')
    attributes = {'name': '', 'urs': '', 'color': ''}

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        return self.post_all_olds_datas(context=context, attributes=self.attributes)


class Update(LoginRequiredMixin, UpdateView, OldDataMixin):
    """Actualiza un tipo de rack"""
    model = RackType
    template_name = 'racks/types/edit.html'
    form_class = RackTypeForm
    success_url = reverse_lazy('racks:type.index')

    def get_attributes(self):
        return {
            'name': self.get_object().name,
            'urs': self.get_object().urs,
            'color': self.get_object().color
        }

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        return self.post_all_olds_datas(context=context, attributes=self.get_attributes())


class Delete(LoginRequiredMixin, DeleteView):
    """Elimina un tipo de rack"""
    model = RackType
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        data = {
            'message': '¡El registro ha sido eliminado correctamente!'
        }
        return JsonResponse(data)
