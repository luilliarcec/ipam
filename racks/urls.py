"""Locations Urls"""

# Django
from django.urls import path

# Views
import racks.brand.views as brand
import racks.model.views as model
import racks.type.views as racktype
import racks.inverter.views as inverter
import racks.battery.views as battery
import racks.rack.views as rack

urlpatterns = [
    path(route='brand', view=brand.Index.as_view(), name='brand.index'),
    path(route='brand/create', view=brand.Create.as_view(), name='brand.store'),
    path(route='brand/edit/<pk>', view=brand.Update.as_view(), name='brand.update'),
    path(route='brand/delete/<pk>', view=brand.Delete.as_view(), name='brand.delete'),

    path(route='model', view=model.Index.as_view(), name='model.index'),
    path(route='model/create', view=model.Create.as_view(), name='model.store'),
    path(route='model/edit/<pk>', view=model.Update.as_view(), name='model.update'),
    path(route='model/delete/<pk>', view=model.Delete.as_view(), name='model.delete'),

    path(route='type', view=racktype.Index.as_view(), name='type.index'),
    path(route='type/create', view=racktype.Create.as_view(), name='type.store'),
    path(route='type/edit/<pk>', view=racktype.Update.as_view(), name='type.update'),
    path(route='type/delete/<pk>', view=racktype.Delete.as_view(), name='type.delete'),

    path(route='inverter', view=inverter.Index.as_view(), name='inverter.index'),
    path(route='inverter/create', view=inverter.Create.as_view(), name='inverter.store'),
    path(route='inverter/edit/<pk>', view=inverter.Update.as_view(), name='inverter.update'),
    path(route='inverter/delete/<pk>', view=inverter.Delete.as_view(), name='inverter.delete'),

    path(route='battery', view=battery.Index.as_view(), name='battery.index'),
    path(route='battery/create', view=battery.Create.as_view(), name='battery.store'),
    path(route='battery/edit/<pk>', view=battery.Update.as_view(), name='battery.update'),
    path(route='battery/delete/<pk>', view=battery.Delete.as_view(), name='battery.delete'),

    path(route='', view=rack.Index.as_view(), name='rack.index'),
    path(route='create', view=rack.Create.as_view(), name='rack.store'),
    path(route='edit/<pk>', view=rack.Update.as_view(), name='rack.update'),
    path(route='delete/<pk>', view=rack.Delete.as_view(), name='rack.delete'),
]
