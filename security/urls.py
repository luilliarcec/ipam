"""Security Urls"""

# Django
from django.urls import path

# Views
import security.views as security

urlpatterns = [
    path(route='', view=security.HomeView.as_view(), name='home'),
    path(route='login/', view=security.LoginView.as_view(), name='login'),
    path(route='logout/', view=security.LogoutView.as_view(), name='logout'),
]
