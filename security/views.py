""" Security Views """

# Django
from django.shortcuts import render
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class HomeView(LoginRequiredMixin, TemplateView):
    """Vista home de la aplicación"""
    template_name = 'auth/home.html'


class LoginView(auth_views.LoginView):
    """Clase Based View para logear un usuario"""
    template_name = 'auth/login.html'
    redirect_authenticated_user = True


class LogoutView(LoginRequiredMixin, auth_views.LogoutView):
    """Clase Based View para logout un usuario"""
    pass
